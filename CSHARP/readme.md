Project Neve: MealPrepSunday
A program adatbázisban tárolja egyes ételek hozzávalóit és tápértékét, majd egy hétre előre étrendet tervez, bevásárlási listát készít és elkészítési időt becsül.
A bevásárló listán szereplő termékeket opcionálisan 3 különböző kategóriába tudja sorolni, aszerint hogy zöldségesnél, hentesnél vagy szupermarketban érdemes megvásárolni azokat.
Felhasználókat szintén adatbázisban tárolja, amiben előre be lehet állítani a személyes preferenciáinkat pl tápanyagok mennyisége, allergiák, vagy húsmentes ételek.