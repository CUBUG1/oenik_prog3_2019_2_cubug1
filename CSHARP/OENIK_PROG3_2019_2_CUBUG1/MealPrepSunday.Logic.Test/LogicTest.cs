﻿// <copyright file="LogicTest.cs" company="lmaoyeet">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MealPrepSunday.Logic.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MealPrepSunday.Data;
    using MealPrepSunday.Logic;
    using MealPrepSunday.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// This class handles the tests for the Logic layer.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Mock<IRepository<Food>> fmock = new Mock<IRepository<Food>>();
        private Mock<IRepository<Ingredient>> imock = new Mock<IRepository<Ingredient>>();
        private Mock<IRepository<Recipe>> rmock = new Mock<IRepository<Recipe>>();
        private Mock<IRepository<Store>> smock = new Mock<IRepository<Store>>();

        private MealPrepLogic logic;

        /// <summary>
        /// Initializ for Moq.
        /// </summary>
        [SetUp]
        public void Init()
        {
            List<Food> foods = new List<Food>();
            List<Ingredient> ingredients = new List<Ingredient>();
            List<Recipe> recipes = new List<Recipe>();
            List<Store> stores = new List<Store>();

            ingredients.Add(new Ingredient()
            {
                Iid = 115,
                Sid = 1,
                Name = "tojás",
                Unit = "db",
                Type = "egyéb",
            });

            ingredients.Add(new Ingredient()
            {
                Iid = 2,
                Sid = 1,
                Name = "zsömle",
                Unit = "db",
                Type = "péksüti",
            });

            ingredients.Add(new Ingredient()
            {
                Iid = 29,
                Sid = 1,
                Name = "paradicsom",
                Unit = "db",
                Type = "zöldség",
            });

            ingredients.Add(new Ingredient()
            {
                Iid = 10,
                Sid = 1,
                Name = "szeletelt kenyér",
                Unit = "db",
                Type = "péksüti",
            });

            ingredients.Add(new Ingredient()
            {
                Iid = 54,
                Sid = 1,
                Name = "sonka",
                Unit = "dkg",
                Type = "hús",
            });

            ingredients.Add(new Ingredient()
            {
                Iid = 71,
                Sid = 1,
                Name = "trapista sajt",
                Unit = "dkg",
                Type = "tejtermék",
            });

            ingredients.Add(new Ingredient()
            {
                Iid = 134,
                Sid = 1,
                Name = "csirkemell",
                Unit = "dkg",
                Type = "hús",
            });

            ingredients.Add(new Ingredient()
            {
                Iid = 160,
                Sid = 1,
                Name = "szárnyasfűszer",
                Unit = "dkg",
                Type = "fűszer",
            });

            ingredients.Add(new Ingredient()
            {
                Iid = 82,
                Sid = 1,
                Name = "halrúd",
                Unit = "db",
                Type = "mirelit",
            });

            ingredients.Add(new Ingredient()
            {
                Iid = 99,
                Sid = 1,
                Name = "basmati rizs",
                Unit = "dkg",
                Type = "egyéb",
            });

            ingredients.Add(new Ingredient()
            {
                Iid = 80,
                Sid = 1,
                Name = "wok zöldség",
                Unit = "dkg",
                Type = "mirelit",
            });

            foods.Add(new Food()
            {
                Fid = 1,
                Name = "kemény tojás",
                Servings = 1,
                BestB4 = 1,
                Type = "reggeli",
                Vega = 1,
            });

            recipes.Add(new Recipe()
            {
                Rid = 1,
                Fid = 1,
                Iid = 115,
                Quantity = 3,
            });

            recipes.Add(new Recipe()
            {
                Rid = 2,
                Fid = 1,
                Iid = 2,
                Quantity = 2,
            });

            recipes.Add(new Recipe()
            {
                Rid = 3,
                Fid = 1,
                Iid = 29,
                Quantity = 1,
            });

            foods.Add(new Food()
            {
                Fid = 2,
                Name = "melegszendvics (sonkás)",
                Servings = 1,
                BestB4 = 1,
                Type = "reggeli",
                Vega = 0,
            });

            recipes.Add(new Recipe()
            {
                Rid = 7,
                Fid = 3,
                Iid = 10,
                Quantity = 4,
            });

            recipes.Add(new Recipe()
            {
                Rid = 8,
                Fid = 3,
                Iid = 54,
                Quantity = 1,
            });

            recipes.Add(new Recipe()
            {
                Rid = 9,
                Fid = 3,
                Iid = 71,
                Quantity = 2,
            });

            recipes.Add(new Recipe()
            {
                Rid = 10,
                Fid = 3,
                Iid = 117,
                Quantity = 0,
            });

            foods.Add(new Food()
            {
                Fid = 3,
                Name = "halrúd",
                Servings = 1,
                BestB4 = 5,
                Type = "feltét",
                Vega = 0,
            });

            recipes.Add(new Recipe()
            {
                Rid = 11,
                Fid = 6,
                Iid = 82,
                Quantity = 6,
            });

            foods.Add(new Food()
            {
                Fid = 4,
                Name = "fűszeres csirkemell",
                Servings = 1,
                BestB4 = 5,
                Type = "feltét",
                Vega = 0,
            });

            recipes.Add(new Recipe()
            {
                Rid = 12,
                Fid = 7,
                Iid = 134,
                Quantity = 3,
            });

            recipes.Add(new Recipe()
            {
                Rid = 13,
                Fid = 7,
                Iid = 160,
                Quantity = 0,
            });

            foods.Add(new Food()
            {
                Fid = 5,
                Name = "basmati rizs",
                Servings = 1,
                BestB4 = 5,
                Type = "köret",
                Vega = 1,
            });

            recipes.Add(new Recipe()
            {
                Rid = 13,
                Fid = 8,
                Iid = 99,
                Quantity = 1,
            });

            foods.Add(new Food()
            {
                Fid = 6,
                Name = "wok zöldség",
                Servings = 1,
                BestB4 = 5,
                Type = "köret",
                Vega = 1,
            });

            recipes.Add(new Recipe()
            {
                Rid = 13,
                Fid = 9,
                Iid = 80,
                Quantity = 1,
            });

            Store store = new Store()
            {
                Sid = 1,
                Name = "Auchan",
                Opentime = new DateTime(1, 1, 1, 8, 00, 00),
                Closetime = new DateTime(1, 1, 1, 19, 00, 00),
                Location = "Budaörs",
            };

            stores.Add(store);

            this.fmock.Setup(m => m.GetAllElements()).Returns(foods.AsQueryable);
            this.imock.Setup(m => m.GetAllElements()).Returns(ingredients.AsQueryable);
            this.rmock.Setup(m => m.GetAllElements()).Returns(recipes.AsQueryable);
            this.smock.Setup(m => m.GetAllElements()).Returns(stores.AsQueryable);
            this.smock.Setup(m => m.GetElementById(1)).Returns(store);
            this.fmock.Setup(m => m.DeleteElement(1));

            // 495. sorban van tesztelve a GetElementById()
            this.logic = new MealPrepLogic(this.fmock.Object, this.imock.Object, this.smock.Object, this.rmock.Object);

            Settings s = new Settings(false, false, 2, 2);
            this.logic.Setting = s;
        }

        /// <summary>
        /// Tests whether the logic throws an exception for an invalid food.
        /// </summary>
        [Test]
        public void CreateInvalidFood()
        {
            Assert.That(() => this.logic.CreateNewFood("asd", 0, 3, "reggeli", 0), Throws.TypeOf<WrongArgumentException>());

            this.fmock.Verify(x => x.AddElement(new Food() { Name = "asd", Fid = 1000, Servings = 0, BestB4 = 1, Vega = 1, Type = "reggeli" }), Times.Never);
        }

        /// <summary>
        /// Tests whether the logic throws an exception for an invalid store.
        /// </summary>
        [Test]
        public void CreateInvalidIngredient()
        {
            Assert.That(() => this.logic.CreateNewIngredient(10, "kávé", "dkg", "egyéb"), Throws.TypeOf<NoSuchItemInDatabaseExcemtion>());
            this.imock.Verify(x => x.AddElement(new Ingredient() { Name = "kávé", Unit = "dkg", Type = "egyéb" }), Times.Never);
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void CreateInvalidStore()
        {
            Assert.That(() => this.logic.CreateNewStore("asd", new DateTime(1, 1, 1, 12, 0, 0), new DateTime(1, 1, 1, 5, 0, 0), "Miskolc"), Throws.TypeOf<WrongArgumentException>());
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void DeleteFoodIdWrongId()
        {
            Assert.That(() => this.logic.DeleteFood(420), Throws.TypeOf<NoSuchItemInDatabaseExcemtion>());
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void DeleteFoodIdGoodId()
        {
            Assert.That(() => this.logic.DeleteFood(3), Throws.Nothing);
            this.fmock.Verify(x => x.DeleteElement(3), Times.Once);
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void IngredientTest()
        {
            var q = this.logic.GetAllIngredients();

            Assert.That(q.Count == 11);
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void FoodTest()
        {
            var q = this.logic.GetAllFoods();

            Assert.That(q.Count == 6);
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void StoreTest()
        {
            var q = this.logic.GetAllStores();

            Assert.That(q.Count == 1);
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void RecepieTest()
        {
            var q = this.logic.GetAllRecipe();

            Assert.That(q.Count == 12);
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void TestIngredientTypes()
        {
            var q = this.logic.IngredientTypes();
            Assert.That(q.Contains("egyéb"));
            Assert.That(q.Contains("péksüti"));
            Assert.That(!q.Contains("gyümölcs"));
            Assert.That(q.Contains("hús"));
            Assert.That(q.Contains("fűszer"));
            Assert.That(!q.Contains("dróg"));
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void CheckIngredientsFromRecipe()
        {
            var q = this.logic.IngredientsFromRecipe(9);
            var list = q.Select(s => new { s.Iid, s.Sid, s.Name }).ToList();
            Assert.That(list[0].Iid == 80);
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void FoodTypesLit()
        {
            var q = this.logic.FoodTypes();
            Assert.That(q.Count == 3);
            Assert.That(q.Contains("reggeli"));
            Assert.That(q.Contains("feltét"));
            Assert.That(q.Contains("köret"));
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void DeleteFoodThatDoesntExist()
        {
            Assert.That(() => this.logic.DeleteFood(99), Throws.TypeOf<NoSuchItemInDatabaseExcemtion>());
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void DeletTest()
        {
            var b4delet = this.logic.GetAllFoods();
            this.logic.DeleteFood(1);
            var afterdelet = this.logic.GetAllFoods();
            Assert.That(b4delet.Count - afterdelet.Count == 0);
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void MakeListOfFoodsTestNoBreakfast()
        {
            var testt = this.logic.MakeListOfFoods();
            Assert.That(testt.Where(x => x.Type == "reggeli").ToList().Count == 0);
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void MakeListOfFoodsTestBreakfast()
        {
            this.logic.Setting.Breakfast = true;
            var testt = this.logic.MakeListOfFoods();
            Assert.That(testt.Where(x => x.Type == "reggeli").ToList().Count == 2);
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void GetRecipeByFIDWithFid3()
        {
            var q = this.logic.GetRecipeByFID(3);
            Assert.That(q.ToList().Count == 4);
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void GetStoreByIdWithid1()
        {
            var q = this.logic.GetStoreById(1); // itt tesztelve van a GetElementById
            Assert.That(q.Sid == 1);
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void GetIngredientsByTypeWithCorrectType()
        {
            var q = this.logic.GetIngredientsByType("egyéb");
            Assert.That(q.Count() == 2);
        }

        /// <summary>
        /// Test.
        /// </summary>
        [Test]
        public void GetIngredientsByTypeWithNotCorrectType()
        {
            var q = this.logic.GetIngredientsByType("dróg");
            Assert.That(q.Count() == 0);
        }
    }
}
