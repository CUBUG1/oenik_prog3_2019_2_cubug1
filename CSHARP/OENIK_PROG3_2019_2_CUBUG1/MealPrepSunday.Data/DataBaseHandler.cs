﻿// <copyright file="DataBaseHandler.cs" company="LmaoYeet">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MealPrepSunday.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// This class handles the database.
    /// </summary>
    public class DatabaseHandler
    {
        static DatabaseHandler()
        {
            DB = new DATA();
        }

        /// <summary>
        /// Gets information from the database.
        /// </summary>
        public static DATA DB { get; }
    }
}
