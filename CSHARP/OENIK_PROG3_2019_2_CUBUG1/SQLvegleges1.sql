﻿IF OBJECT_ID('Recipe', 'U') IS NOT NULL DROP TABLE Recipe;
IF OBJECT_ID('Ingredient', 'U') IS NOT NULL DROP TABLE Ingredient;
IF OBJECT_ID('Store', 'U') IS NOT NULL DROP TABLE Store;
IF OBJECT_ID('Food', 'U') IS NOT NULL DROP TABLE Food;

CREATE TABLE Food 
    (Fid                NUMERIC(5) NOT NULL,
     Name               VARCHAR(30),
     Servings           NUMERIC(1),
     BestB4             NUMERIC(1),
     Type               VARCHAR(10),
     Vega               NUMERIC(1),
  CONSTRAINT f_pk PRIMARY KEY (Fid));

CREATE TABLE Store
    (Sid                NUMERIC(5) NOT NULL,
     Name               VARCHAR(30),
     Opentime           DATETIME,
     Closetime          DATETIME,
     Location           VARCHAR(50),
  CONSTRAINT s_pk PRIMARY KEY(Sid));

  CREATE TABLE Ingredient
    (Iid                NUMERIC (5) NOT NULL,
     Sid                NUMERIC(5),
     Name               VARCHAR(30),
     Unit               VARCHAR(15),
	   Type				        VARCHAR(10),
  CONSTRAINT i_pk PRIMARY KEY(Iid),
  CONSTRAINT i_fk FOREIGN KEY (Sid) REFERENCES Store (Sid));

  CREATE TABLE Recipe
    (Rid NUMERIC(5) NOT NULL,
	 Fid NUMERIC(5) NOT NULL,
     Iid NUMERIC(5) NOT NULL,
     Quantity NUMERIC(5,2),
  CONSTRAINT r_pk PRIMARY KEY(Rid),
  CONSTRAINT r_fk1 FOREIGN KEY(Fid) REFERENCES Food(Fid),
  CONSTRAINT r_fk2 FOREIGN KEY(Iid) REFERENCES Ingredient(Iid));


INSERT INTO Store VALUES(00001,'Hipermarket',convert(datetime, '07:00:00', 8),convert(datetime, '21:00:00', 8),'2040 Budaörs Sport u. 2 - 4.');
INSERT INTO Store VALUES(00002,'Hentes',convert(datetime, '07:00:00', 8),convert(datetime, '17:00:00', 8),'2040 Budaörs Károly Király u. 145.');
INSERT INTO Store VALUES(00003,'Zödséges',convert(datetime, '07:00:00', 8), convert(datetime, '13:00:00', 8),'2040 Budaörs Szabadság út 174.');

--hozzávalók
--pésüti
INSERT INTO Ingredient VALUES (00001,00001,'kifli','db','péksüti');
INSERT INTO Ingredient VALUES (00002,00001,'zsömle','db','péksüti');
INSERT INTO Ingredient VALUES (00003,00001,'kenyér','kg','péksüti');
INSERT INTO Ingredient VALUES (00004,00001,'sajtos pogácsa','db','péksüti');
INSERT INTO Ingredient VALUES (00005,00001,'sajtos stangli','db','péksüti');
INSERT INTO Ingredient VALUES (00006,00001,'tepertős pogácsa','db','péksüti');
INSERT INTO Ingredient VALUES (00007,00001,'vajas croissant','db','péksüti');
INSERT INTO Ingredient VALUES (00008,00001,'kakaós csiga','db','péksüti');
INSERT INTO Ingredient VALUES (00009,00001,'pizzás csiga','db','péksüti');
INSERT INTO Ingredient VALUES (00010,00001,'túrós táska','db','péksüti');
INSERT INTO Ingredient VALUES (00011,00001,'szeletelt kenyér','szelet','péksüti');
INSERT INTO Ingredient VALUES (00012,00003,'alma','db','gyümölcs');
INSERT INTO Ingredient VALUES (00013,00003,'banán','db','gyümölcs');
INSERT INTO Ingredient VALUES (00014,00003,'narancs','db','gyümölcs');
INSERT INTO Ingredient VALUES (00015,00003,'citrom','db','gyümölcs');
INSERT INTO Ingredient VALUES (00016,00003,'lime','db','gyümölcs');
INSERT INTO Ingredient VALUES (00017,00003,'szilva','kg','gyümölcs');
INSERT INTO Ingredient VALUES (00018,00003,'körte','db','gyümölcs');
INSERT INTO Ingredient VALUES (00019,00003,'szőlő','dkg','gyümölcs');
INSERT INTO Ingredient VALUES (00020,00003,'ananász','db','gyümölcs');
INSERT INTO Ingredient VALUES (00021,00003,'kivi','db','gyümölcs');
INSERT INTO Ingredient VALUES (00022,00003,'eper','dkg','gyümölcs');
INSERT INTO Ingredient VALUES (00023,00003,'málna','dkg','gyümölcs');
INSERT INTO Ingredient VALUES (00024,00003,'gránátalma','db','gyümölcs');
INSERT INTO Ingredient VALUES (00025,00003,'sárgabarack','db','gyümölcs');
INSERT INTO Ingredient VALUES (00026,00003,'őszibarack','db','gyümölcs');
INSERT INTO Ingredient VALUES (00027,00003,'sárgadinnye','db','gyümölcs');
INSERT INTO Ingredient VALUES (00028,00003,'paprika','db','zöldség');
INSERT INTO Ingredient VALUES (00029,00003,'paradicsom','db','zöldség');
INSERT INTO Ingredient VALUES (00030,00003,'lilahagyma','db','zöldség');
INSERT INTO Ingredient VALUES (00031,00003,'vöröshagyma','db','zöldség');
INSERT INTO Ingredient VALUES (00032,00003,'padlizsán','db','zöldség');
INSERT INTO Ingredient VALUES (00033,00003,'cukkini','db','zöldség');
INSERT INTO Ingredient VALUES (00034,00003,'burgonya','kg','zöldség');
INSERT INTO Ingredient VALUES (00035,00003,'cékla','db','zöldség');
INSERT INTO Ingredient VALUES (00036,00003,'édesburgonya','dkg','zöldség');
INSERT INTO Ingredient VALUES (00037,00003,'karfiol','db','zöldség');
INSERT INTO Ingredient VALUES (00038,00003,'karalábé','db','zöldség');
INSERT INTO Ingredient VALUES (00039,00003,'retek','db','zöldség');
INSERT INTO Ingredient VALUES (00040,00003,'sárgrépa','db','zöldség');
INSERT INTO Ingredient VALUES (00041,00003,'fehérrépa','kg','zöldség');
INSERT INTO Ingredient VALUES (00042,00003,'spenót','dkg','zöldség');
INSERT INTO Ingredient VALUES (00043,00003,'sütőtök','db','zöldség');
INSERT INTO Ingredient VALUES (00044,00003,'uborka','dkg','zöldség');
INSERT INTO Ingredient VALUES (00045,00003,'salátakeverék','db','zöldség');
INSERT INTO Ingredient VALUES (00046,00003,'fokhagyma','db','zöldség');
INSERT INTO Ingredient VALUES (00047,00001,'napraforgó olaj','dl','olaj');
INSERT INTO Ingredient VALUES (00048,00001,'kukorica olaj','dl','olaj');
INSERT INTO Ingredient VALUES (00049,00001,'olivaolaj','dl','olaj');
INSERT INTO Ingredient VALUES (00050,00001,'ecet','dl','olaj');
INSERT INTO Ingredient VALUES (00051,00001,'virsli','db','felvágott');
INSERT INTO Ingredient VALUES (00052,00001,'kolbász','dkg','felvágott');
INSERT INTO Ingredient VALUES (00053,00001,'párizsi','dkg','felvágott');
INSERT INTO Ingredient VALUES (00054,00001,'sonka','dkg','felvágott');
INSERT INTO Ingredient VALUES (00055,00001,'debreceni','db','felvágott');
INSERT INTO Ingredient VALUES (00056,00001,'barna csiperke','dkg','gomba');
INSERT INTO Ingredient VALUES (00057,00001,'fehér csiperke','dkg','gomba');
INSERT INTO Ingredient VALUES (00058,00001,'shiitake','dkg','gomba');
INSERT INTO Ingredient VALUES (00059,00001,'laska','dkg','gomba');
INSERT INTO Ingredient VALUES (00060,00001,'portobello','dkg','gomba');
INSERT INTO Ingredient VALUES (00061,00001,'tej (kék)','dl','tejtermék');
INSERT INTO Ingredient VALUES (00062,00001,'tej (piros)','dl','tejtermék');
INSERT INTO Ingredient VALUES (00063,00001,'tej (fekete)','l','tejtermék');
INSERT INTO Ingredient VALUES (00064,00001,'laktózmentes tej','dl','tejtermék');
INSERT INTO Ingredient VALUES (00065,00001,'natúr joghurt','db','tejtermék');
INSERT INTO Ingredient VALUES (00066,00001,'gyümölcsös joghurt','db','tejtermék');
INSERT INTO Ingredient VALUES (00067,00001,'kefír','dl','tejtermék');
INSERT INTO Ingredient VALUES (00068,00001,'görög joghurt','db','tejtermék');
INSERT INTO Ingredient VALUES (00069,00001,'tejföl','g','tejtermék');
INSERT INTO Ingredient VALUES (00070,00001,'túró','dkg','tejtermék');
INSERT INTO Ingredient VALUES (00071,00001,'trapista sajt','dkg','tejtermék');
INSERT INTO Ingredient VALUES (00072,00001,'karaván sajt','dkg','tejtermék');
INSERT INTO Ingredient VALUES (00073,00001,'kecskesajt','dkg','tejtermék');
INSERT INTO Ingredient VALUES (00074,00001,'mozzarella','zacskó','tejtermék');
INSERT INTO Ingredient VALUES (00075,00001,'vaj','dkg','tejtermék');
INSERT INTO Ingredient VALUES (00076,00001,'margarin','dkg','tejtermék');
INSERT INTO Ingredient VALUES (00077,00001,'tejszín','dl','tejtermék');
INSERT INTO Ingredient VALUES (00078,00001,'mirelit zöldségkeverés','zacskó','mirelit');
INSERT INTO Ingredient VALUES (00079,00001,'mirelit brokkoli','dkg','mirelit');
INSERT INTO Ingredient VALUES (00080,00001,'wok zöldség','zacskó','mirelit');
INSERT INTO Ingredient VALUES (00081,00001,'mirelit spenót','doboz','mirelit');
INSERT INTO Ingredient VALUES (00082,00001,'halrudak','db','mirelit');
INSERT INTO Ingredient VALUES (00083,00001,'pizza','db','mirelit');
INSERT INTO Ingredient VALUES (00084,00001,'fagyi','doboz','mirelit');
INSERT INTO Ingredient VALUES (00085,00001,'hasábburgonya','zacskó','mirelit');
INSERT INTO Ingredient VALUES (00086,00001,'morzsolt kukorica','konzerv','tartós');
INSERT INTO Ingredient VALUES (00087,00001,'passzírozott paradicsom','doboz','tartós');
INSERT INTO Ingredient VALUES (00088,00001,'vörösbab','konzerv','tartós');
INSERT INTO Ingredient VALUES (00089,00001,'zöldborsó','konzerv','tartós');
INSERT INTO Ingredient VALUES (00090,00001,'fehérbab','konzerv','tartós');
INSERT INTO Ingredient VALUES (00091,00001,'lencse','konzerv','tartós');
INSERT INTO Ingredient VALUES (00092,00001,'kristálycukor','dkg','egyéb');
INSERT INTO Ingredient VALUES (00093,00001,'nádcukor','dkg','egyéb');
INSERT INTO Ingredient VALUES (00094,00001,'porcukor','dkg','egyéb');
INSERT INTO Ingredient VALUES (00095,00001,'só','kg','egyéb');
INSERT INTO Ingredient VALUES (00096,00001,'jázmin rizs','zacskó','egyéb');
INSERT INTO Ingredient VALUES (00097,00001,'barna rizs','zacskó','egyéb');
INSERT INTO Ingredient VALUES (00098,00001,'fehér rizs','zacskó','egyéb');
INSERT INTO Ingredient VALUES (00099,00001,'basmati rizs','zacskó','egyéb');
INSERT INTO Ingredient VALUES (00100,00001,'búza finomliszt','dkg','egyéb');
INSERT INTO Ingredient VALUES (00101,00001,'búza rétesliszt','dkg','egyéb');
INSERT INTO Ingredient VALUES (00102,00001,'búzadara','dkg','egyéb');
INSERT INTO Ingredient VALUES (00103,00001,'zabliszt','dkg','egyéb');
INSERT INTO Ingredient VALUES (00104,00001,'rizsliszt','dkg','egyéb');
INSERT INTO Ingredient VALUES (00105,00001,'teljes kiörlésű búzaliszt','dkg','egyéb');
INSERT INTO Ingredient VALUES (00106,00001,'tönkölybúzaliszt','dkg','egyéb');
INSERT INTO Ingredient VALUES (00107,00001,'köles','dkg','egyéb');
INSERT INTO Ingredient VALUES (00108,00001,'zabpehely','dkg','egyéb');
INSERT INTO Ingredient VALUES (00109,00001,'gabonapehely','dkg','egyéb');
INSERT INTO Ingredient VALUES (00110,00001,'kukoricapehely','dkg','egyéb');
INSERT INTO Ingredient VALUES (00111,00001,'müzli','dkg','egyéb');
INSERT INTO Ingredient VALUES (00112,00001,'lekvár','db','egyéb');
INSERT INTO Ingredient VALUES (00113,00001,'méz','db','egyéb');
INSERT INTO Ingredient VALUES (00114,00001,'nutella','dkg','egyéb');
INSERT INTO Ingredient VALUES (00115,00001,'tojás','db','egyéb');
INSERT INTO Ingredient VALUES (00116,00001,'kakaópor','g','egyéb');
INSERT INTO Ingredient VALUES (00117,00001,'ketchup','g','egyéb');
INSERT INTO Ingredient VALUES (00118,00001,'majonéz','g','egyéb');
INSERT INTO Ingredient VALUES (00119,00001,'mustár','g','egyéb');
INSERT INTO Ingredient VALUES (00120,00001,'zsemlemorzsa','dkg','egyéb');
INSERT INTO Ingredient VALUES (00121,00001,'kukoricadara','dkg','egyéb');
INSERT INTO Ingredient VALUES (00122,00001,'mandulabél','g','mag');
INSERT INTO Ingredient VALUES (00123,00001,'földimogyoró','g','mag');
INSERT INTO Ingredient VALUES (00124,00001,'kesudió','g','mag');
INSERT INTO Ingredient VALUES (00125,00001,'pisztácia','g','mag');
INSERT INTO Ingredient VALUES (00126,00001,'pekándió','g','mag');
INSERT INTO Ingredient VALUES (00127,00001,'spaghetti','g','tészta');
--
INSERT INTO Ingredient VALUES (00129,00001,'penne','g','tészta');
INSERT INTO Ingredient VALUES (00130,00001,'farfalle','g','tészta');
INSERT INTO Ingredient VALUES (00131,00001,'fusilli','g','tészta');
INSERT INTO Ingredient VALUES (00132,00001,'maccheroni','g','tészta');
INSERT INTO Ingredient VALUES (00133,00001,'lasegne','g','tészta');
INSERT INTO Ingredient VALUES (00134,00002,'csirke mellfilé','dkg','hús');
INSERT INTO Ingredient VALUES (00135,00002,'csirke máj','dkg','hús');
INSERT INTO Ingredient VALUES (00136,00002,'csirke comb (egész)','db','hús');
INSERT INTO Ingredient VALUES (00137,00002,'csirke comb (felső)','db','hús');
INSERT INTO Ingredient VALUES (00138,00002,'csirke comb (alsó)','db','hús');
INSERT INTO Ingredient VALUES (00139,00002,'csirke comb (filé)','db','hús');
INSERT INTO Ingredient VALUES (00140,00002,'csirke szárny','dkg','hús');
INSERT INTO Ingredient VALUES (00141,00002,'csirke nyak','dkg','hús');
INSERT INTO Ingredient VALUES (00142,00002,'sertés karaj','dkg','hús');
INSERT INTO Ingredient VALUES (00143,00002,'sertés tarja','dkg','hús');
INSERT INTO Ingredient VALUES (00144,00002,'sertés fehérpecsenye','dkg','hús');
INSERT INTO Ingredient VALUES (00145,00002,'sertés darálthús','dkg','hús');
INSERT INTO Ingredient VALUES (00146,00002,'sertés oldalas','dkg','hús');
INSERT INTO Ingredient VALUES (00147,00002,'sertés szűzpecsenye','dkg','hús');
INSERT INTO Ingredient VALUES (00148,00002,'marha gulyáshús','dkg','hús');
INSERT INTO Ingredient VALUES (00149,00002,'marha lábszár','dkg','hús');
INSERT INTO Ingredient VALUES (00150,00002,'marha nyak','dkg','hús');
INSERT INTO Ingredient VALUES (00151,00001,'füstölt lazac','dkg','hal');
INSERT INTO Ingredient VALUES (00152,00001,'garnéla','dkg','hal');
INSERT INTO Ingredient VALUES (00153,00001,'ponty','dkg','hal');
INSERT INTO Ingredient VALUES (00154,00001,'afrikai harcsa','dkg','hal');
INSERT INTO Ingredient VALUES (00155,00001,'pisztráng','dkg','hal');
INSERT INTO Ingredient VALUES (00156,00001,'tőkehal','dkg','hal');
INSERT INTO Ingredient VALUES (00157,00001,'lazacfilé','dkg','hal');
INSERT INTO Ingredient VALUES (00158,00001,'afrikai harcsa','dkg','hal');
INSERT INTO Ingredient VALUES (00159,00001,'pirospaprika','g','fűszer');
INSERT INTO Ingredient VALUES (00160,00001,'szárnyas fűszerkeverék','csomag','fűszer');
INSERT INTO Ingredient VALUES (00161,00001,'mexikói fűszerkeverék','csomag','fűszer');
INSERT INTO Ingredient VALUES (00162,00001,'olasz fűszerkeverék','csomag','fűszer');
INSERT INTO Ingredient VALUES (00163,00001,'petrezselyem','csomag','fűszer');
INSERT INTO Ingredient VALUES (00164,00001,'kakukkfű','csomag','fűszer');
INSERT INTO Ingredient VALUES (00165,00001,'bazsalikom','csomag','fűszer');
INSERT INTO Ingredient VALUES (00166,00001,'rozmaring','csomag','fűszer');
INSERT INTO Ingredient VALUES (00167,00001,'chilli','csomag','fűszer');
INSERT INTO Ingredient VALUES (00168,00001,'narancslé','l','ital');
INSERT INTO Ingredient VALUES (00169,00001,'almalé','l','ital');
INSERT INTO Ingredient VALUES (00170,00001,'baracklé','l','ital');
INSERT INTO Ingredient VALUES (00171,00001,'kóla','l','ital');
INSERT INTO Ingredient VALUES (00172,00001,'jegestea','l','ital');
INSERT INTO Ingredient VALUES (00173,00001,'ásványvíz','l','ital');
INSERT INTO Ingredient VALUES (00174,00001,'sör','dl','ital');
INSERT INTO Ingredient VALUES (00175,00001,'vörösbor','dl','ital');
INSERT INTO Ingredient VALUES (00176,00001,'fehérbor','l','ital');

--Kaják
--reggelik
INSERT INTO Food VALUES(00001,'kemény tojás',1,1,'reggeli',1);
INSERT INTO Recipe VALUES(00001,00001,00115,3);--tojás
INSERT INTO Recipe VALUES(00002,00001,00002,2);--zsömle
INSERT INTO Recipe VALUES(00003,00001,00029,1);--paradicsom

INSERT INTO Food VALUES(00002,'rántotta',1,1,'reggeli',1);
INSERT INTO Recipe VALUES(00004,00002,00115,3);--tojás
INSERT INTO Recipe VALUES(00005,00002,00002,2);--zsömle
INSERT INTO Recipe VALUES(00006,00002,00029,1);--paradicsom

INSERT INTO Food VALUES(00003,'zabkása (banános)', 1, 1,'reggeli',1);
INSERT INTO Recipe VALUES(00007,00003,00013,1);--banán
INSERT INTO Recipe VALUES(00008,00003,00108,5);--zabpehely
INSERT INTO Recipe VALUES(00009,00003,00064,3);--laktimentes tej

INSERT INTO Food VALUES(00004,'zabkása (kakós)',1,1,'reggeli',1);
INSERT INTO Recipe VALUES(00010,00004,00116,NULL);--kakaópor
INSERT INTO Recipe VALUES(00011,00004,00108,5);--zabpehely
INSERT INTO Recipe VALUES(00012,00004,00064,3);--laktimentes tej

INSERT INTO Food VALUES(00005,'zabkása (málnás)',1,1,'reggeli',1);
INSERT INTO Recipe VALUES(00013,00005,00023,3);--málna
INSERT INTO Recipe VALUES(00014,00005,00108,5);--zabpehely
INSERT INTO Recipe VALUES(00015,00005,00064,3);--laktimentes tej

INSERT INTO Food VALUES(00006,'zabkása (almás)',1,1,'reggeli',1);
INSERT INTO Recipe VALUES(00016,00006,00012,1);--alma
INSERT INTO Recipe VALUES(00017,00006,00108,5);--zabpehely
INSERT INTO Recipe VALUES(00018,00006,00064,3);--laktimentes tej

INSERT INTO Food VALUES(00007,'zabkása (epres)',1,1,'reggeli',1);
INSERT INTO Recipe VALUES(00019,00007,00022,3);--eper
INSERT INTO Recipe VALUES(00020,00007,00108,5);--zabpehely
INSERT INTO Recipe VALUES(00021,00007,00064,3);--laktimentes tej

INSERT INTO Food VALUES(00008,'zabkása (nutellás)',1,1,'reggeli',1);
INSERT INTO Recipe VALUES(00022,00008,00114,NULL);--nutella
INSERT INTO Recipe VALUES(00023,00008,00108,5);--zabpehely
INSERT INTO Recipe VALUES(00024,00008,00064,3);--laktimentes tej

INSERT INTO Food VALUES(00009,'melegszendvics (sonkás)',1,1,'reggeli',0);
INSERT INTO Recipe VALUES(00025,00009,00010,4);--szeletelt kenyér
INSERT INTO Recipe VALUES(00026,00009,00054,1);--sonka
INSERT INTO Recipe VALUES(00027,00009,00071,2);--trapista
INSERT INTO Recipe VALUES(00028,00009,00117,NULL);--ketchup

INSERT INTO Food VALUES(00010,'melegszendvics (kolbászos)',1,1,'reggeli',0);
INSERT INTO Recipe VALUES(00029,00010,00010,4);--szeletelt kenyér
INSERT INTO Recipe VALUES(00030,00010,00052,1);--kolbász
INSERT INTO Recipe VALUES(00031,00010,00071,2);--trapista
INSERT INTO Recipe VALUES(00032,00010,00117,NULL);--ketchup

INSERT INTO Food VALUES(00011,'melegszendvics (párizsis)',1,1,'reggeli',0);
INSERT INTO Recipe VALUES(00033,00011,00010,4);--szeletelt kenyér
INSERT INTO Recipe VALUES(00034,00011,00053,1);--párizsi
INSERT INTO Recipe VALUES(00035,00011,00071,2);--trapista
INSERT INTO Recipe VALUES(00036,00011,00117,NULL);--ketchup

INSERT INTO Food VALUES(00012,'melegszendvics (sajtos)',1,1,'reggeli',1);
INSERT INTO Recipe VALUES(00037,00012,00010,4);--szeletelt kenyér
INSERT INTO Recipe VALUES(00038,00012,00072,2);--karaván
INSERT INTO Recipe VALUES(00039,00012,00071,2);--trapista
INSERT INTO Recipe VALUES(00040,00012,00117,NULL);--ketchup

INSERT INTO Food VALUES(00013,'szendvics (sonkás)',1,1,'reggeli',0);
INSERT INTO Recipe VALUES(00041,00013,00005,2);--sajtos stangli
INSERT INTO Recipe VALUES(00042,00013,00054,1);--sonka
INSERT INTO Recipe VALUES(00043,00013,00071,2);--trapista
INSERT INTO Recipe VALUES(00044,00013,00076,NULL);--margrin
INSERT INTO Recipe VALUES(00045,00013,00028,1);--paprika

INSERT INTO Food VALUES(00014,'szendvics (kolbászos)',1,1,'reggeli',0);
INSERT INTO Recipe VALUES(00046,00014,00005,2);--sajtos stangli
INSERT INTO Recipe VALUES(00047,00014,00052,1);--kolbász
INSERT INTO Recipe VALUES(00048,00014,00071,2);--trapista
INSERT INTO Recipe VALUES(00049,00014,00076,NULL);--margrin
INSERT INTO Recipe VALUES(00050,00014,00028,1);--paprika

INSERT INTO Food VALUES(00015,'szendvics (párizsis)',1,1,'reggeli',0);
INSERT INTO Recipe VALUES(00051,00015,00005,2);--sajtos stangli
INSERT INTO Recipe VALUES(00052,00015,00053,1);--párizsi
INSERT INTO Recipe VALUES(00053,00015,00071,2);--trapista
INSERT INTO Recipe VALUES(00054,00015,00076,NULL);--margrin
INSERT INTO Recipe VALUES(00055,00015,00028,1);--paprik

INSERT INTO Food VALUES(00016,'szendvics (sajtos)',1,1,'reggeli',1);
INSERT INTO Recipe VALUES(00056,00016,00005,2);--sajtos stangli
INSERT INTO Recipe VALUES(00057,00016,00072,2);--karaván
INSERT INTO Recipe VALUES(00058,00016,00071,2);--trapista
INSERT INTO Recipe VALUES(00059,00016,00076,NULL);--margrin
INSERT INTO Recipe VALUES(00060,00016,00028,1);--paprika

INSERT INTO Food VALUES(00017,'virsli',1,1,'reggeli',0);
INSERT INTO Recipe VALUES(00061,00017,00002,2);--zsömle
INSERT INTO Recipe VALUES(00062,00017,00051,4);--virsli
INSERT INTO Recipe VALUES(00063,00017,00029,1);--paradicsom
INSERT INTO Recipe VALUES(00064,00017,00119,NULL);--mustár

INSERT INTO Food VALUES(00018,'debreceni',1,1,'reggeli',0);
INSERT INTO Recipe VALUES(00065,00018,00002,2);--zsömle
INSERT INTO Recipe VALUES(00066,00018,00055,2);--debreceni
INSERT INTO Recipe VALUES(00067,00018,00029,1);--paradicsom
INSERT INTO Recipe VALUES(00068,00018,00119,NULL);--mustár

INSERT INTO Food VALUES(00019,'gyümlcsös joghurt',1,1,'reggeli',1);
INSERT INTO Recipe VALUES(00069,00019,00001,1);--kifli
INSERT INTO Recipe VALUES(00070,00019,00066,1);--gyümijoghurt
INSERT INTO Recipe VALUES(00071,00019,00012,1);--alma

INSERT INTO Food VALUES(00020,'görög joghurt',1,1,'reggeli',1);
INSERT INTO Recipe VALUES(00072,00020,00001,1);--kifli
INSERT INTO Recipe VALUES(00073,00020,00066,1);--görög joghurt
INSERT INTO Recipe VALUES(00074,00020,00013,1);--banán

INSERT INTO Food VALUES(00021,'körözött',2,1,'reggeli',1);
INSERT INTO Recipe VALUES(00075,00021,00005,4);--sajtos stangli
INSERT INTO Recipe VALUES(00076,00021,00069,200);--tejföl
INSERT INTO Recipe VALUES(00077,00021,00070,250);--túró
INSERT INTO Recipe VALUES(00078,00021,00030,1);--lilahagyma
INSERT INTO Recipe VALUES(00079,00021,00159,NULL);--pirospaprika

INSERT INTO Food VALUES(00022,'péksüti',1,1,'reggeli',1);
INSERT INTO Recipe VALUES(00080,00022,00007,4);--vajas croissant
INSERT INTO Recipe VALUES(00081,00022,00062,2);--tej
INSERT INTO Recipe VALUES(00082,00022,00076,NULL);--margrin

INSERT INTO Food VALUES(00023,'kifli+kefir',1,1,'reggeli',1);
INSERT INTO Recipe VALUES(00083,00023,00001,2);--kifli
INSERT INTO Recipe VALUES(00084,00023,00067,2);--kefir
INSERT INTO Recipe VALUES(00085,00023,00012,1);--alma

--ebédek
INSERT INTO Food VALUES(00024,'fűszeres csirkemell',1,5,'feltét',0);
INSERT INTO Recipe VALUES(00086,00024,00134,2);--csirkemell
INSERT INTO Recipe VALUES(00087,00024,00160,NULL);--szárnyasfűszer

INSERT INTO Food VALUES(00025,'natúr csirkemell',1,5,'feltét',0);
INSERT INTO Recipe VALUES(00088,00025,00134,2);--csirkemell
INSERT INTO Recipe VALUES(00089,00025,00075,NULL);--vaj

INSERT INTO Food VALUES(00026,'mézes-mustáros tarja',1,5,'feltét',0);
INSERT INTO Recipe VALUES(00090,00026,00143,2);--tarja
INSERT INTO Recipe VALUES(00091,00026,00113,NULL);--méz
INSERT INTO Recipe VALUES(00092,00026,00119,NULL);--mustár
INSERT INTO Recipe VALUES(00093,00026,00047,NULL);--olaj
INSERT INTO Recipe VALUES(00094,00026,00046,NULL);--fokhagyma

INSERT INTO Food VALUES(00027,'cukkini fasírt',1,5,'feltét',1);
INSERT INTO Recipe VALUES(00095,00027,00115,1);--tojás
INSERT INTO Recipe VALUES(00096,00027,00033,1);--cukkini
INSERT INTO Recipe VALUES(00097,00027,00046,NULL);--fokhagyma
INSERT INTO Recipe VALUES(00098,00027,00071,10);--trapista sajt
INSERT INTO Recipe VALUES(00099,00027,00100,3);--list
INSERT INTO Recipe VALUES(00140,00027,00120,NULL);--zsemlemorzsa
INSERT INTO Recipe VALUES(00140,00027,00047,NULL);--olaj

INSERT INTO Food VALUES(00028,'tojásos rizs(kukoricás)',2,5,'főétel',1);
INSERT INTO Recipe VALUES(00101,00028,00115,2);--tojás
INSERT INTO Recipe VALUES(00102,00028,00096,1);--rizs
INSERT INTO Recipe VALUES(00103,00028,00086,1);--konzerv kukorica
INSERT INTO Recipe VALUES(00104,00028,00074,1);--mozzarella

INSERT INTO Food VALUES(00029,'tojásos rizs(wokos)',2,5,'főétel',1);
INSERT INTO Recipe VALUES(00105,00029,00115,2);--tojás
INSERT INTO Recipe VALUES(00106,00029,00096,1);--rizs
INSERT INTO Recipe VALUES(00107,00029,00080,1);--wok zöldség
INSERT INTO Recipe VALUES(00108,00029,00074,1);--mozzarella

INSERT INTO Food VALUES(00030,'tojásos rizs(vegyes zöldséges)',2,5,'főétel',1);
INSERT INTO Recipe VALUES(00109,00030,00115,2);--tojás
INSERT INTO Recipe VALUES(00140,00030,00096,1);--rizs
INSERT INTO Recipe VALUES(00111,00030,00078,1);--vegyes zöld
INSERT INTO Recipe VALUES(00112,00030,00074,1);--mozzarella

INSERT INTO Food VALUES(00031,'spenótos tészta',2,3,'tészta',1);
INSERT INTO Recipe VALUES(00113,00031,00081,1);--spenót
INSERT INTO Recipe VALUES(00114,00031,00115,1);--tojás
INSERT INTO Recipe VALUES(00115,00031,00069,NULL);--tejföl
INSERT INTO Recipe VALUES(00116,00031,00046,NULL);--fokhagyma
INSERT INTO Recipe VALUES(00117,00031,00131,250);--fusilli
INSERT INTO Recipe VALUES(00118,00031,00047,NULL);--olaj
INSERT INTO Recipe VALUES(00119,00031,00103,NULL);--zablisz

INSERT INTO Food VALUES(00032,'chilis bab',2,5,'főétel',0);
INSERT INTO Recipe VALUES(00140,00032,00145,3);--sertés darálthús
INSERT INTO Recipe VALUES(00121,00032,00049,NULL);--olivaolaj
INSERT INTO Recipe VALUES(00122,00032,00031,1);--vöröshagyma
INSERT INTO Recipe VALUES(00123,00032,00046,NULL);--fokhagyma
INSERT INTO Recipe VALUES(00124,00032,00087,1);--paradicsomlé
INSERT INTO Recipe VALUES(00125,00032,00086,1);--kukorica
INSERT INTO Recipe VALUES(00126,00032,00088,1);--vörösbab
INSERT INTO Recipe VALUES(00127,00032,00161,1);--mexikói fűszer

INSERT INTO Food VALUES(00033,'gombás tejszínes tészta',1,3,'tészta',1);
INSERT INTO Recipe VALUES(00128,00033,00057,1);--csiperke
INSERT INTO Recipe VALUES(00129,00033,00077,1);--tejszín
INSERT INTO Recipe VALUES(00140,00033,00049,NULL);--olivaolaj
INSERT INTO Recipe VALUES(00131,00033,00046,NULL);--fokhagyma
INSERT INTO Recipe VALUES(00132,00033,00164,NULL);--kakukkfű
INSERT INTO Recipe VALUES(00133,00033,00071,1);--trapista
INSERT INTO Recipe VALUES(00134,00033,00163,NULL);--petrezselyem
INSERT INTO Recipe VALUES(00135,00033,00129,150);--penne

INSERT INTO Food VALUES(00034,'halrúd',1,5,'feltét',0);
INSERT INTO Recipe VALUES(00136,00034,00082,6);--halrúd
INSERT INTO Recipe VALUES(00137,00034,00047,NULL);--olaj

INSERT INTO Food VALUES(00035,'jázmin rizs',1,5,'köret',1);
INSERT INTO Recipe VALUES(00138,00035,00096,1);--jázmin rizs

INSERT INTO Food VALUES(00036,'basmati rizs',1,5,'köret',1);
INSERT INTO Recipe VALUES(00139,00036,00099,1);--basmati rizs

INSERT INTO Food VALUES(00037,'barna rizs',1,5,'köret',1);
INSERT INTO Recipe VALUES(00141,00037,00097,1);--barna rizs

INSERT INTO Food VALUES(00038,'fehér rizs',1,5,'köret',1);
INSERT INTO Recipe VALUES(00142,00038,00098,1);--fehér rizs

INSERT INTO Food VALUES(00039,'wok zöldség',2,5,'köret',1);
INSERT INTO Recipe VALUES(00143,00039,00080,1);--wok zöldég

INSERT INTO Food VALUES(00040,'vegyes zöldség',2,5,'köret',1);
INSERT INTO Recipe VALUES(00144,00040,00078,1);--vegyes zöld

INSERT INTO Food VALUES(00041,'hasáb burgonya',1,5,'köret',1);
INSERT INTO Recipe VALUES(00145,00041,00085,NULL);--hasáb

INSERT INTO Food VALUES(00042,'vegyes saláta',2,5,'köret',1);
INSERT INTO Recipe VALUES(00146,00042,00045,1);--salátakeverék

INSERT INTO Food VALUES(00043,'édesburgonya',2,5,'köret',1);
INSERT INTO Recipe VALUES(00147,00043,00036,5);--édesburgonya
INSERT INTO Recipe VALUES(00148,00043,00049,NULL);--olivaolaj
INSERT INTO Recipe VALUES(00149,00043,00167,NULL);--chili
