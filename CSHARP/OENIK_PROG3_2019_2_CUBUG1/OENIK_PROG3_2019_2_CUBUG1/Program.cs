﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OENIK_PROG3_2019_2_CUBUG1
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MealPrepSunday.Data;
    using MealPrepSunday.Logic;
    using MealPrepSunday.Repository;

    /// <summary>
    /// This class holds the logic for the project.
    /// </summary>
    internal class Program
    {
        /// <summary>Enum for menu.</summary>
        public enum MenuState
        {
            /// <summary>Main menu state</summary>
            MainMenu,

            /// <summary>state where all the meals listed</summary>
            MealsListed,

            /// <summary>state where the list generation happnes</summary>
            GenerateList,

            /// <summary>something something</summary>
            SelectNewFood,

            /// <summary>option where you can select the type to list</summary>
            SelectIngredientType,

            /// <summary>option where you can select a specific ingredient in the selected type</summary>
            SelectIngredient,

            /// <summary>state where the edting happens</summary>
            EditIngredient,

            /// <summary>state where adding ingredient happens</summary>
            AddIngredient,

            /// <summary>menu option where you can select a store</summary>
            SelectStore,

            /// <summary>add new store</summary>
            AddStore,

            /// <summary>edit selected store</summary>
            EditStore,

            /// <summary>menu option where you can edit the settings</summary>
            Settings,

            /// <summary>useless menuoption just to satisfy the project requirements</summary>
            Java,

            /// <summary>exit</summary>
            Exit,
        }

        /// <summary>
        /// This class holds the logic for the project.
        /// </summary>
        /// <param name="args">args.</param>
        public static void Main()
        {
            MenuState menu = MenuState.MainMenu;
            ConsoleKeyInfo pressedButton;
            Settings settings = new Settings() { Vega = true, Breakfast = true, MaxMelasRepeats = 2, MaxNumberOfMeals = 5 };
            IMealPrepLogic logic =
                new MealPrepLogic(
                new FoodRepository(),
                new IngredientRepository(),
                new StoreRepository(),
                new RecipeRepository(),
                settings);

            List<Food> foods = new List<Food>();
            List<string> ingredientTypes = logic.IngredientTypes();
            List<Store> stores = logic.GetAllStores();

            // variables for input
            string inputString = string.Empty;
            int sid = 0;
            DateTime open = DateTime.MinValue;
            DateTime close = DateTime.MinValue;
            string loc = string.Empty;
            string name = string.Empty;
            string unit = string.Empty;
            string type = string.Empty;
            bool correctAnswer = true;
            string selectedType = string.Empty;
            List<Ingredient> ingredientsByType = null;
            Ingredient selectedIngredient = null;
            Store selectedStore = null;
            while (menu != MenuState.Exit)
            {
                switch (menu)
                {
                    case MenuState.MainMenu:
                        DrawMainMenu();
                        correctAnswer = true;
                        do
                        {
                            pressedButton = Console.ReadKey();
                            correctAnswer = true;
                            if (pressedButton.Key == ConsoleKey.D1)
                            {
                                menu = MenuState.MealsListed;
                            }
                            else if (pressedButton.Key == ConsoleKey.D2)
                            {
                                menu = MenuState.SelectIngredientType;
                            }
                            else if (pressedButton.Key == ConsoleKey.D3)
                            {
                                menu = MenuState.Java;
                            }
                            else if (pressedButton.Key == ConsoleKey.D4)
                            {
                                menu = MenuState.SelectStore;
                            }
                            else if (pressedButton.Key == ConsoleKey.D5)
                            {
                                menu = MenuState.Settings;
                            }
                            else if (pressedButton.Key == ConsoleKey.D6)
                            {
                                menu = MenuState.Exit;
                            }
                            else if (pressedButton.Key == ConsoleKey.X)
                            {
                                menu = MenuState.Exit;
                            }
                            else
                            {
                                correctAnswer = false;
                                Console.WriteLine("Rossz választ adott meg");
                                System.Threading.Thread.Sleep(1000);
                            }
                        }
                        while (correctAnswer == false);

                        break;
                    case MenuState.MealsListed:
                        do
                        {
                            foods = logic.MakeListOfFoods();
                            ListMeals(foods);
                            WriteToBottomRow("A: Bevásárlási lista generálása, B: vissza a főmenübe, R: ujragenerálás");
                            Console.SetCursorPosition(0, Console.WindowHeight - 2);
                            pressedButton = Console.ReadKey();
                            correctAnswer = true;
                            if (pressedButton.Key == ConsoleKey.A)
                            {
                                menu = MenuState.GenerateList;
                            }
                            else if (pressedButton.Key == ConsoleKey.B)
                            {
                                menu = MenuState.MainMenu;
                            }
                            else if (pressedButton.Key == ConsoleKey.R)
                            {
                                foods = null;
                                menu = MenuState.MealsListed;
                            }
                            else
                            {
                                correctAnswer = false;
                                Console.WriteLine("Rossz választ adott meg");
                                System.Threading.Thread.Sleep(1000);
                            }
                        }
                        while (correctAnswer == false);
                        break;
                    case MenuState.GenerateList:
                        List<ShoppingListElement> shoppingList = logic.CreateShoppingList(foods);
                        WriteShoppinglistToFile(shoppingList);
                        menu = MenuState.MainMenu;
                        break;
                    case MenuState.SelectNewFood:
                        break;
                    case MenuState.SelectIngredientType:
                        do
                        {
                            correctAnswer = true;
                            Console.Clear();
                            ingredientTypes = logic.IngredientTypes();
                            for (int i = 0; i < ingredientTypes.Count(); i++)
                            {
                                Console.WriteLine("{0}. {1}", i + 1, ingredientTypes[i]);
                            }

                            WriteToBottomRow("SZÁM: Típus kiválasztása, A: Hozzáadás B: vissza a főmenübe");
                            Console.SetCursorPosition(0, Console.WindowHeight - 2);
                            inputString = Console.ReadLine();
                            if (inputString == null || inputString.Length == 0)
                            {
                                correctAnswer = false;
                                Console.WriteLine("Nem adott választ");
                                System.Threading.Thread.Sleep(1000);
                            }
                            else if (inputString[0] == 'B' || inputString[0] == 'b')
                            {
                                menu = MenuState.MainMenu;
                            }
                            else if (inputString[0] == 'A' || inputString[0] == 'a')
                            {
                                menu = MenuState.AddIngredient;
                            }
                            else if (int.Parse(inputString) < ingredientTypes.Count() + 1)
                            {
                                selectedType = ingredientTypes[int.Parse(inputString) - 1];
                                menu = MenuState.SelectIngredient;
                            }
                            else
                            {
                                correctAnswer = false;
                                Console.WriteLine("Rossz választ adott meg");
                                System.Threading.Thread.Sleep(1000);
                            }
                        }
                        while (correctAnswer == false);

                        break;
                    case MenuState.SelectIngredient:
                        ingredientsByType = logic.GetIngredientsByType(selectedType);
                        do
                        {
                            correctAnswer = true;
                            Console.Clear();
                            for (int i = 0; i < ingredientsByType.Count(); i++)
                            {
                                Console.WriteLine("{0}. {1}", i + 1, ingredientsByType[i].Name);
                            }

                            WriteToBottomRow("SZÁM: Hozzávaló kiválasztása, B: vissza a főmenübe");
                            Console.SetCursorPosition(0, Console.WindowHeight - 2);
                            inputString = Console.ReadLine();
                            if (inputString == null || inputString.Length == 0)
                            {
                                correctAnswer = false;
                                Console.WriteLine("Nem adott választ");
                                System.Threading.Thread.Sleep(1000);
                            }
                            else if (inputString[0] == 'B' || inputString[0] == 'b')
                            {
                                menu = MenuState.MainMenu;
                            }
                            else if (int.Parse(inputString) < ingredientsByType.Count() + 1)
                            {
                                selectedIngredient = ingredientsByType[int.Parse(inputString) - 1];
                                menu = MenuState.EditIngredient;
                            }
                            else
                            {
                                correctAnswer = false;
                                Console.WriteLine("Rossz választ adott meg");
                                System.Threading.Thread.Sleep(1000);
                            }
                        }
                        while (correctAnswer == false);

                        break;
                    case MenuState.EditIngredient:
                        do
                        {
                            correctAnswer = true;
                            Console.Clear();
                            Console.SetCursorPosition(0, 0);
                            DrawEditIngredient(selectedIngredient);
                            Console.SetCursorPosition(0, Console.WindowHeight - 2);
                            pressedButton = Console.ReadKey();
                            if (pressedButton.Key == ConsoleKey.A)
                            {
                                menu = MenuState.AddIngredient;
                            }
                            else if (pressedButton.Key == ConsoleKey.E)
                            {
                                Console.Clear();
                                Console.SetCursorPosition(0, 0);
                                Console.Write("Régi név: {0}, Új név: ", selectedIngredient.Name);
                                name = Console.ReadLine();
                                Console.Write("Régi típus: {0}, Új típus: ", selectedIngredient.Type);
                                type = Console.ReadLine();
                                Console.Write("Régi mértékegység: {0}, Új mértékegység: ", selectedIngredient.Unit);
                                unit = Console.ReadLine();
                                Console.Write("Régi bolti azonosító: {0}, Új bolti azonosító: ", selectedIngredient.Sid);
                                sid = int.Parse(Console.ReadLine());
                                logic.UpdateIngredient((int)selectedIngredient.Iid, sid, name, unit, type);
                                Console.WriteLine("Hozzávaló frissítve");
                                System.Threading.Thread.Sleep(1000);
                                selectedIngredient = null;
                                menu = MenuState.SelectIngredientType;
                            }
                            else if (pressedButton.Key == ConsoleKey.R)
                            {
                                try
                                {
                                    logic.DeleteIngredient((int)selectedIngredient.Iid);
                                }
                                catch (System.Data.Entity.Infrastructure.DbUpdateException)
                                {
                                    Console.Clear();
                                    Console.WriteLine("Elemet nem lehet törölni, mert szerepel egy receptben");
                                    System.Threading.Thread.Sleep(2000);
                                }
                                catch (NoSuchItemInDatabaseExcemtion)
                                {
                                    Console.WriteLine("ez az elem nem létezik");
                                }
                                catch (Exception e)
                                {
                                    Console.Clear();
                                    Console.WriteLine(e.Message);
                                    Console.WriteLine("Upsza valami hiba történt");
                                    System.Threading.Thread.Sleep(2000);
                                }

                                selectedIngredient = null;
                                menu = MenuState.SelectIngredientType;
                            }
                            else if (pressedButton.Key == ConsoleKey.B)
                            {
                                menu = MenuState.MainMenu;
                            }
                            else
                            {
                                correctAnswer = false;
                                Console.WriteLine("Rossz választ adott meg");
                                System.Threading.Thread.Sleep(1000);
                            }
                        }
                        while (correctAnswer == false);
                        break;
                    case MenuState.AddIngredient:
                        Console.Clear();
                        Console.Write("Név: ");
                        name = Console.ReadLine();
                        Console.Write("Mértékegység: ");
                        unit = Console.ReadLine();
                        Console.Write("Típus: ");
                        type = Console.ReadLine();
                        Console.Write("Bolt id: ");
                        sid = int.Parse(Console.ReadLine());
                        Console.WriteLine("Biztos hozzá szeretné adni?");
                        do
                        {
                            WriteToBottomRow("A hozzáad B vissza");
                            pressedButton = Console.ReadKey();
                            correctAnswer = true;
                            if (pressedButton.Key == ConsoleKey.A)
                            {
                                logic.CreateNewIngredient(sid, name, unit, type);
                                menu = MenuState.MainMenu;
                            }
                            else if (pressedButton.Key == ConsoleKey.B)
                            {
                                menu = MenuState.MainMenu;
                            }
                            else
                            {
                                correctAnswer = false;
                                Console.WriteLine("Rossz választ adott meg");
                                System.Threading.Thread.Sleep(1000);
                            }
                        }
                        while (correctAnswer == false);
                        break;
                    case MenuState.SelectStore:
                        do
                        {
                            correctAnswer = true;
                            stores = logic.GetAllStores();
                            for (int i = 0; i < stores.Count(); i++)
                            {
                                Console.WriteLine("{0}. {1}", i + 1, stores[i].Name);
                            }

                            WriteToBottomRow("SZÁM: Bolt kiválasztása, A: Hozzáadás B: vissza a főmenübe");
                            Console.SetCursorPosition(0, Console.WindowHeight - 2);
                            inputString = Console.ReadLine();
                            if (inputString == null || inputString.Length == 0)
                            {
                                correctAnswer = false;
                                Console.WriteLine("Nem adott választ");
                                System.Threading.Thread.Sleep(1000);
                            }
                            else if (inputString[0] == 'A' || inputString[0] == 'a')
                            {
                                menu = MenuState.AddStore;
                            }
                            else if (inputString[0] == 'B' || inputString[0] == 'b')
                            {
                                menu = MenuState.MainMenu;
                            }
                            else if (int.Parse(inputString) < stores.Count() + 1)
                            {
                                selectedStore = stores[int.Parse(inputString) - 1];
                                menu = MenuState.EditStore;
                            }
                            else
                            {
                                correctAnswer = false;
                                Console.WriteLine("Rossz választ adott meg");
                                System.Threading.Thread.Sleep(1000);
                            }
                        }
                        while (correctAnswer == false);
                        break;
                    case MenuState.AddStore:
                        Console.Clear();
                        Console.Write("Név: ");
                        name = Console.ReadLine();
                        Console.Write("Nyitás: ");
                        open = DateTime.Parse(Console.ReadLine());
                        Console.Write("Zárás: ");
                        close = DateTime.Parse(Console.ReadLine());
                        Console.Write("Cím: ");
                        loc = Console.ReadLine();
                        Console.WriteLine("Biztos hozzá szeretné adni?");
                        do
                        {
                            WriteToBottomRow("A hozzáad B vissza");
                            pressedButton = Console.ReadKey();
                            correctAnswer = true;
                            if (pressedButton.Key == ConsoleKey.A)
                            {
                                logic.CreateNewStore(name, open, close, loc);
                                menu = MenuState.MainMenu;
                            }
                            else if (pressedButton.Key == ConsoleKey.B)
                            {
                                menu = MenuState.MainMenu;
                            }
                            else
                            {
                                correctAnswer = false;
                                Console.WriteLine("Rossz választ adott meg");
                                System.Threading.Thread.Sleep(1000);
                            }
                        }
                        while (correctAnswer == false);
                        break;
                    case MenuState.EditStore:
                        do
                        {
                            correctAnswer = true;
                            Console.Clear();
                            Console.SetCursorPosition(0, 0);
                            DrawEditStore(selectedStore);
                            Console.SetCursorPosition(0, Console.WindowHeight - 2);
                            pressedButton = Console.ReadKey();
                            if (pressedButton.Key == ConsoleKey.A)
                            {
                                menu = MenuState.AddStore;
                            }
                            else if (pressedButton.Key == ConsoleKey.E)
                            {
                                Console.Clear();
                                Console.SetCursorPosition(0, 0);
                                Console.Write("Régi név: {0}, Új név: ", selectedStore.Name);
                                name = Console.ReadLine();
                                if (name.Length == 0 || name == null)
                                {
                                    name = selectedStore.Name;
                                }

                                Console.Write("Régi nyitás: {0}:{1}, Új típus: ", selectedStore.Opentime.Value.Hour, selectedStore.Opentime.Value.Minute);
                                string unformattedOpen = Console.ReadLine();
                                if (unformattedOpen.Length == 0 || unformattedOpen == null)
                                {
                                    open = (DateTime)selectedStore.Opentime;
                                }
                                else
                                {
                                    open = DateTime.Parse(unformattedOpen);
                                }

                                Console.Write("Régi zárás: {0}:{1}, Új mértékegység: ", selectedStore.Opentime.Value.Hour, selectedStore.Opentime.Value.Minute);
                                string unformattedClose = Console.ReadLine();
                                if (unformattedClose.Length == 0 || unformattedClose == null)
                                {
                                    close = (DateTime)selectedStore.Closetime;
                                }
                                else
                                {
                                    close = DateTime.Parse(unformattedOpen);
                                }

                                Console.Write("Régi cím: {0}, Új bolti azonosító: ", selectedStore.Location);
                                loc = Console.ReadLine();
                                if (loc.Length == 0 || loc == null)
                                {
                                    loc = selectedStore.Location;
                                }

                                logic.UpdateStore((int)selectedStore.Sid, name, open, close, loc);
                                Console.WriteLine("Bolt frissítve");
                                System.Threading.Thread.Sleep(1000);
                                selectedStore = null;
                                menu = MenuState.SelectStore;
                            }
                            else if (pressedButton.Key == ConsoleKey.R)
                            {
                                try
                                {
                                    logic.DeleteStore((int)selectedStore.Sid);
                                }
                                catch (System.Data.Entity.Infrastructure.DbUpdateException)
                                {
                                    Console.Clear();
                                    Console.WriteLine("Elemet nem lehet törölni, mert szerepel egy receptben");
                                    System.Threading.Thread.Sleep(2000);
                                }
                                catch (NoSuchItemInDatabaseExcemtion)
                                {
                                    Console.WriteLine("ez az elem nem létezik");
                                }
                                catch (Exception e)
                                {
                                    Console.Clear();
                                    Console.WriteLine(e.Message);
                                    Console.WriteLine("Upsza valami hiba történt");
                                    System.Threading.Thread.Sleep(2000);
                                }

                                selectedStore = null;
                                menu = MenuState.SelectStore;
                            }
                            else if (pressedButton.Key == ConsoleKey.B)
                            {
                                menu = MenuState.MainMenu;
                            }
                            else
                            {
                                correctAnswer = false;
                                Console.WriteLine("Rossz választ adott meg");
                                System.Threading.Thread.Sleep(1000);
                            }
                        }
                        while (correctAnswer == false);
                        break;
                    case MenuState.Settings:
                        Console.WriteLine("Settings");
                        correctAnswer = true;
                        do
                        {
                            DrawSettings(settings);
                            correctAnswer = true;
                            pressedButton = Console.ReadKey();
                            if (pressedButton.Key == ConsoleKey.D1)
                            {
                                settings.Vega = !settings.Vega;
                            }
                            else if (pressedButton.Key == ConsoleKey.D2)
                            {
                                settings.Breakfast = !settings.Breakfast;
                            }
                            else if (pressedButton.Key == ConsoleKey.D3)
                            {
                                settings.MaxMelasRepeats += 1;
                            }
                            else if (pressedButton.Key == ConsoleKey.D4)
                            {
                                settings.MaxNumberOfMeals += 1;
                            }
                            else if (pressedButton.Key == ConsoleKey.B)
                            {
                                menu = MenuState.MainMenu;
                            }
                            else
                            {
                                correctAnswer = false;
                                Console.WriteLine("Rossz választ adott meg");
                                System.Threading.Thread.Sleep(1000);
                            }
                        }
                        while (correctAnswer == false);

                        break;
                    case MenuState.Java:
                        Console.Clear();
                        Console.WriteLine("Username? (tokmind1)");
                        string username = Console.ReadLine();
                        Recept randomRecept = ReceptGyujto.GetReceptAsync(username).Result;
                        Console.WriteLine("{0} {1} {2}", randomRecept.Name, randomRecept.Quantity, randomRecept.Unit);
                        Console.ReadKey();
                        break;
                    default:
                        menu = MenuState.Exit;
                        break;
                }

                Console.Clear();
            }
        }

        /// <summary>
        /// draws to console.
        /// </summary>
        private static void DrawMainMenu()
        {
            Console.WriteLine("Mealprep Sunday Főmenü: ");
            Console.WriteLine("1. Étrend összeállítása");
            Console.WriteLine("2. Hozzávalók szerkesztése");
            Console.WriteLine("3. Java");
            Console.WriteLine("4. Boltok szerkesztése");
            Console.WriteLine("5. Beállítások");
            Console.WriteLine("6. Kilépés");
        }

        /// <summary>
        /// draws to console.
        /// <param name="s">Setting.</param>
        /// </summary>
        private static void DrawSettings(Settings s)
        {
            Console.WriteLine("Beállítások: ");
            Console.WriteLine("1. Vegetáriánus: {0} ", s.Vega);
            Console.WriteLine("2. Reggelik összeállítása: {0}", s.Breakfast);
            Console.WriteLine("3. Max ismétlés: {0}", s.MaxMelasRepeats);
            Console.WriteLine("4. Generált ételek száma: {0}", s.MaxNumberOfMeals);
            Console.WriteLine("B. Vissza");
        }

        /// <summary>
        /// draws to console.
        /// <param name="selected">selected ingredient.</param>
        /// </summary>
        private static void DrawEditIngredient(Ingredient selected)
        {
            Console.WriteLine("Név:            {0}", selected.Name);
            Console.WriteLine("Mértékegység:   {0}", selected.Unit);
            Console.WriteLine("Típus:          {0}", selected.Type);
            Console.WriteLine("Bolt azonosító: {0}", selected.Sid);
            WriteToBottomRow("A: Új hozzáadása, E: Szerkesztés, R: Törlés, B: vissza a főmenübe");
        }

        /// <summary>
        /// draws to console.
        /// <param name="selected">selected Store.</param>
        /// </summary>
        private static void DrawEditStore(Store selected)
        {
            Console.WriteLine("Név:            {0}", selected.Name);
            Console.WriteLine("Nyit:           {0}:{1}", selected.Opentime.Value.Hour, selected.Opentime.Value.Minute);
            Console.WriteLine("Zár:            {0}:{1}", selected.Closetime.Value.Hour, selected.Opentime.Value.Minute);
            Console.WriteLine("Cím:            {0}", selected.Location);
            WriteToBottomRow("A: Új hozzáadása, E: Szerkesztés, R: Törlés, B: vissza a főmenübe");
        }

        /// <summary>
        /// draws to console.
        /// <param name="fs">input list.</param>
        /// </summary>
        private static void ListMeals(List<Food> fs)
        {
            int i;
            for (i = 0; i < fs.Count(); i++)
            {
                Console.WriteLine("{0}. {1}", i + 1, fs[i].Name);
            }
        }

        /// <summary>
        /// draws to console.
        /// <param name="s">string write to bottom.</param>
        /// </summary>
        private static void WriteToBottomRow(string s)
        {
            try
            {
                Console.SetCursorPosition(Console.WindowWidth - s.Length - 5, Console.WindowHeight - 2);
                Console.Write(s);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// draws to console.
        /// <param name="s">list to convert.</param>
        /// </summary>
        private static void WriteShoppinglistToFile(List<ShoppingListElement> s)
        {
            System.IO.StreamWriter write = new System.IO.StreamWriter("shoppinglist.txt");
            foreach (ShoppingListElement tmp in s)
            {
                write.Write(tmp.Name);
                for (int i = 0; i < 40 - tmp.Name.Length - tmp.Unit.Length - tmp.Quantity.ToString().Length; i++)
                {
                    write.Write(".");
                }

                write.WriteLine("{0} {1}", tmp.Quantity, tmp.Unit);
            }

            write.Close();
        }
    }
}
