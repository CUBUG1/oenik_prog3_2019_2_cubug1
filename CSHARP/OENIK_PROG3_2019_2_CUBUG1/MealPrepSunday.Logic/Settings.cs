﻿// <copyright file="Settings.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MealPrepSunday.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// This class stores the current settings.
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Gets or sets a value indicating whether this class stores the current settings.
        /// </summary>
        public bool Vega { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this class stores the current settings.
        /// </summary>
        public bool Breakfast { get; set; }

        /// <summary>
        /// Gets or sets Max number of each meals.
        /// </summary>
        public int MaxMelasRepeats
        {
            get
            {
                return this.maxMealsRepeats;
            }

            set
            {
                if (value >= this.maxNumberOfMeals || value < 1)
                {
                    this.maxMealsRepeats = 1;
                }
                else
                {
                    this.maxMealsRepeats = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets THe amount of meals created by the program.
        /// </summary>
        public int MaxNumberOfMeals
        {
            get
            {
                return this.maxNumberOfMeals;
            }

            set
            {
                if (value > 14 || value < 1)
                {
                    this.maxNumberOfMeals = 1;
                }
                else
                {
                    this.maxNumberOfMeals = value;
                }
            }
        }

        private int maxMealsRepeats;

        private int maxNumberOfMeals;

        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        public Settings()
        {
            this.Vega = false;
            this.Breakfast = true;
            this.MaxMelasRepeats = 2;
            this.MaxNumberOfMeals = 7;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Settings"/> class.
        /// </summary>
        /// <param name="v">Vegeterian.</param>
        /// <param name="b">Breakfast.</param>
        /// <param name="mr">MaxMealRepeats.</param>
        /// <param name="mm">MaximumNumberOfMeals.</param>
        public Settings(bool v, bool b, int mr, int mm)
        {
            this.Vega = v;
            this.Breakfast = b;
            this.MaxMelasRepeats = mr;
            this.MaxNumberOfMeals = mm;
        }
    }
}
