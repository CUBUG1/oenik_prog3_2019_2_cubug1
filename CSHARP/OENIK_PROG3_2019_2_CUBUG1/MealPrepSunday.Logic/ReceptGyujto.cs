﻿// <copyright file="ReceptGyujto.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MealPrepSunday.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json;

    /// <summary>
    /// Class neded for Java.
    /// </summary>
    public static class ReceptGyujto
    {
        private static HttpClient client = new HttpClient();

        /// <summary>
        /// Async collects stuff from web.
        /// </summary>
        /// <param name="username">somethingsomething.</param>
        /// <returns>list of food.</returns>
        public static async Task<Recept> GetReceptAsync(string username)
        {
            Recept recept = null;

            HttpResponseMessage response = await client.GetAsync(string.Format("http://localhost:8080/JavaVegpont/Provider?UserName={0}", username));

            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                recept = JsonConvert.DeserializeObject<Recept>(jsonString);
            }

            return recept;
        }
   }
}
