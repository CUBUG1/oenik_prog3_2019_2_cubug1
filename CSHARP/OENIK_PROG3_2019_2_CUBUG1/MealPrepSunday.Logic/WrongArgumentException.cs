﻿// <copyright file="WrongArgumentException.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MealPrepSunday.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// simple exceptsion for this project.
    /// </summary>
    [Serializable]
    public class WrongArgumentException : Exception
    {
        private readonly string errorMessege;

        /// <summary>
        /// Initializes a new instance of the <see cref="WrongArgumentException"/> class.
        /// </summary>
        /// <param name="error">error.</param>
        public WrongArgumentException(string error)
        {
            this.errorMessege = error;
        }
    }
}
