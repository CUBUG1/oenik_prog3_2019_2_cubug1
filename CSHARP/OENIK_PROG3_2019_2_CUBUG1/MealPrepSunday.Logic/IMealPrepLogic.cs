﻿// <copyright file="IMealPrepLogic.cs" company="lmaoYeet">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MealPrepSunday.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using MealPrepSunday.Data;

    /// <summary>
    /// Interface.
    /// </summary>
    public interface IMealPrepLogic
    {
        /// <summary>
        /// Creates a new food and sends it to the repository.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="servings">servings.</param>
        /// <param name="bestb4">bestb4.</param>
        /// <param name="type">type.</param>
        /// <param name="vega">vega.</param>
        void CreateNewFood(string name, int servings, int bestb4, string type, int vega);

        /// <summary>
        /// Creates a new ingredient and sends it to the repository.
        /// </summary>
        /// <param name="sid">sid.</param>
        /// <param name="name">name.</param>
        /// <param name="unit">unit.</param>
        /// <param name="type">type.</param>
        void CreateNewIngredient(int sid, string name, string unit, string type);

        /// <summary>
        /// Creates a new list of food based on the settings parameters.
        /// </summary>
        /// <returns>List of food types.</returns>
        List<Food> MakeListOfFoods();

        /// <summary>
        /// Creates a new shopping based on fs.
        /// </summary>
        /// <param name="fs">list of foods.</param>
        /// <returns>List of food types.</returns>
        List<ShoppingListElement> CreateShoppingList(List<Food> fs);

        /// <summary>
        /// Creates a new shopping based on fs.
        /// </summary>
        /// <param name="id">list of id.</param>
        /// <returns>true if id exists.</returns>
        bool FindFoodByID(int id);

        /// <summary>
        /// Creates a new shopping based on fs.
        /// </summary>
        /// <param name="id">list of id.</param>
        /// <returns>true if id exists.</returns>
        bool FindIngredientByID(int id);

        /// <summary>
        /// Creates a new shopping based on fs.
        /// </summary>
        /// <param name="id">list of id.</param>
        /// <returns>true if id exists.</returns>
        bool FindRecipeByID(int id);

        /// <summary>
        /// Creates a new shopping based on fs.
        /// </summary>
        /// <param name="id">list of id.</param>
        /// <returns>true if id exists.</returns>
        bool FindStoreByID(int id);

        /// <summary>
        /// Creates a new shopping based on fs.
        /// </summary>
        /// <param name="type">list of id.</param>
        /// <returns>All ingredients where type equals to input parameter.</returns>
        List<Ingredient> GetIngredientsByType(string type);

        /// <summary>
        /// Creates a new recipe and sends it to the repository.
        /// </summary>
        /// <param name="fid">fid.</param>
        /// <param name="iid">iid.</param>
        /// <param name="quantity">quantity.</param>
        void CreateNewRecipe(int fid, int iid, int quantity);

        /// <summary>
        /// Creates a new store and sends it to the repository.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="open">open.</param>
        /// <param name="close">close.</param>
        /// <param name="location">loction.</param>
        void CreateNewStore(string name, DateTime open, DateTime close, string location);

        /// <summary>
        /// Sends a request to repository to delete a food.
        /// </summary>
        /// <param name="id">id.</param>
        void DeleteFood(int id);

        /// <summary>
        /// Sends a request to repository to delete an Ingredient.
        /// </summary>
        /// <param name="id">id.</param>
        void DeleteIngredient(int id);

        /// <summary>
        /// Sends a request to repository to delete a Recipe.
        /// </summary>
        /// <param name="id">id.</param>
        void DeleteRecepie(int id);

        /// <summary>
        /// Sends a request to repository to delete a store.
        /// </summary>
        /// <param name="id">id.</param>
        void DeleteStore(int id);

        /// <summary>
        /// Creates  list.
        /// </summary>
        /// <returns>Lit of food types.</returns>
        List<string> FoodTypes();

        /// <summary>
        /// Helper method to get all the food and converts them to a list.
        /// </summary>
        /// <returns>list of food.</returns>
        List<Food> GetAllFoods();

        /// <summary>
        /// Helper method to get all the Ingredient and converts them to a list.
        /// </summary>
        /// <returns>list of Ingredient.</returns>
        List<Ingredient> GetAllIngredients();

        /// <summary>
        /// Helper method to get all the Recipe and converts them to a list.
        /// </summary>
        /// <returns>list of Recipe.</returns>
        List<Recipe> GetAllRecipe();

        /// <summary>
        /// Helper method to get all the Store and converts them to a list.
        /// </summary>
        /// <returns>list of Store.</returns>
        List<Store> GetAllStores();

        /// <summary>
        /// Sends a request to repository to get all data of a food based on it's id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>food.</returns>
        Food GetFoodById(int id);

        /// <summary>
        /// Sends a request to repository to get all data of a ingredient based on it's id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>food.</returns>
        Ingredient GetIngredientById(int id);

        /// <summary>
        /// Sends a request to repository to get all data of a recipe based on it's id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>food.</returns>
        Recipe GetRecipeById(int id);

        /// <summary>
        /// Sends a request to repository to get all data of a store based on it's id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>food.</returns>
        Store GetStoreById(int id);

        /// <summary>
        /// Creates  list.
        /// </summary>
        /// <param name="foodId">id.</param>
        /// <returns>ingredients from  recipe.</returns>
        IQueryable<Ingredient> IngredientsFromRecipe(int foodId);

        /// <summary>
        /// Creates  list.
        /// </summary>
        /// <returns>Lit of ingredient types.</returns>
        List<string> IngredientTypes();

        /// <summary>
        /// Send a request to repository to update a food in the database.
        /// </summary>
        /// <param name="id">oldid.</param>
        /// <param name="name">name.</param>
        /// <param name="servings">servcing.</param>
        /// <param name="bestb4">bestb4.</param>
        /// <param name="type">type.</param>
        /// <param name="vega">vega.</param>
        void UpdateFood(int id, string name, int servings, int bestb4, string type, int vega);

        /// <summary>
        /// Send a request to repository to update a ingredient in the database.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="sid">sid.</param>
        /// <param name="name">namne.</param>
        /// <param name="unit">unit.</param>
        /// <param name="type">type.</param>
        void UpdateIngredient(int id, int sid, string name, string unit, string type);

        /// <summary>
        /// Send a request to repository to update a Recipe in the database.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="fid">sid.</param>
        /// <param name="iid">namne.</param>
        /// <param name="quantity">unit.</param>
        void UpdateRecipe(int id, int fid, int iid, int quantity);

        /// <summary>
        /// Send a request to repository to update a Recipe in the database.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="name">name,.</param>
        /// <param name="open">open.</param>
        /// <param name="close">close.</param>
        /// <param name="location">loction.</param>
        void UpdateStore(int id, string name, DateTime open, DateTime close, string location);
    }
}