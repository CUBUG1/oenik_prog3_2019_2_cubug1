﻿// <copyright file="NoSuchItemInDatabaseExcemtion.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MealPrepSunday.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// A simple Exception for this project.
    /// </summary>
    [Serializable]
    public class NoSuchItemInDatabaseExcemtion : Exception
    {
        /// <summary>
        /// Gets or sets errorMessege.
        /// </summary>
        private readonly string errorMessege;

        /// <summary>
        /// Initializes a new instance of the <see cref="NoSuchItemInDatabaseExcemtion"/> class.
        /// </summary>
        /// <param name="error">Vegeterian.</param>
        public NoSuchItemInDatabaseExcemtion(string error)
        {
            this.errorMessege = error;
        }
    }
}
