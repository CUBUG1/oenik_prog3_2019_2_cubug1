﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MealPrepSunday.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MealPrepSunday.Data;
    using MealPrepSunday.Logic;
    using MealPrepSunday.Repository;

    /// <summary>
    /// This class holds the logic for the project.
    /// </summary>
    public class MealPrepLogic : IMealPrepLogic
    {
        private IRepository<Food> frepo;
        private IRepository<Ingredient> irepo;
        private IRepository<Store> srepo;
        private IRepository<Recipe> rrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="MealPrepLogic"/> class.
        /// </summary>
        /// <param name="frepo">Repository of Food database.</param>
        /// <param name="irepo">Repository of Ingredient database.</param>
        /// <param name="srepo">Repository of Store database.</param>
        /// <param name="rrepo">Repository of Recipe database.</param>
        public MealPrepLogic(
            IRepository<Food> frepo,
            IRepository<Ingredient> irepo,
            IRepository<Store> srepo,
            IRepository<Recipe> rrepo)
        {
            this.frepo = frepo;
            this.irepo = irepo;
            this.srepo = srepo;
            this.rrepo = rrepo;
        }

        /// <summary>
        /// Creates a new food and sends it to the repository.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="servings">servings.</param>
        /// <param name="bestb4">bestb4.</param>
        /// <param name="type">type.</param>
        /// <param name="vega">vega.</param>
        public void CreateNewFood(string name, int servings, int bestb4, string type, int vega)
        {
            if (name.Length == 0 || servings == 0)
            {
                throw new WrongArgumentException("Food cant be created");
            }
            else
            {
                Food itemToAdd = new Food()
                {
                    Fid = this.frepo.LastId() + 1,
                    Name = name,
                    Servings = servings,
                    BestB4 = bestb4,
                    Type = type,
                    Vega = vega,
                };
                this.frepo.AddElement(itemToAdd);
            }
        }

        /// <summary>
        /// Creates a new ingredient and sends it to the repository.
        /// </summary>
        /// <param name="sid">sid.</param>
        /// <param name="name">name.</param>
        /// <param name="unit">unit.</param>
        /// <param name="type">type.</param>
        public void CreateNewIngredient(int sid, string name, string unit, string type)
        {
            if (!this.srepo.FindId(sid))
            {
                throw new NoSuchItemInDatabaseExcemtion("The store does not exists yet in db");
            }
            else if (name.Length == 0 || unit.Length == 0)
            {
                throw new WrongArgumentException("Ingredient cant be created");
            }
            else
            {
                Ingredient itemToAdd = new Ingredient()
                {
                    Iid = this.irepo.LastId() + 1,
                    Sid = sid,
                    Name = name,
                    Unit = unit,
                    Type = type,
                };
                this.irepo.AddElement(itemToAdd);
            }
        }

        /// <summary>
        /// Creates a new recipe and sends it to the repository.
        /// </summary>
        /// <param name="fid">fid.</param>
        /// <param name="iid">iid.</param>
        /// <param name="quantity">quantity.</param>
        public void CreateNewRecipe(int fid, int iid, int quantity)
        {
            if (!this.frepo.FindId(fid))
            {
                throw new NoSuchItemInDatabaseExcemtion("This food does not exists in db yet");
            }
            else if (!this.irepo.FindId(iid))
            {
                throw new NoSuchItemInDatabaseExcemtion("This ingredient does not exists in db yet");
            }
            else
            {
                Recipe itemToAdd = new Recipe()
                {
                    Rid = this.rrepo.LastId() + 1,
                    Fid = fid,
                    Iid = iid,
                    Quantity = quantity,
                };
                this.rrepo.AddElement(itemToAdd);
            }
        }

        /// <summary>
        /// Creates a new store and sends it to the repository.
        /// </summary>
        /// <param name="name">name.</param>
        /// <param name="open">open.</param>
        /// <param name="close">close.</param>
        /// <param name="location">loction.</param>
        public void CreateNewStore(string name, DateTime open, DateTime close, string location)
        {
            TimeSpan openhours = close - open;

            if (name.Length == 0)
            {
                throw new WrongArgumentException("Store cant be created with this name");
            }
            else if (openhours.Hours <= 0)
            {
                throw new WrongArgumentException("Open cant be greater than close");
            } else
            {
                Store itemToAdd = new Store()
                {
                    Sid = this.srepo.LastId() + 1,
                    Name = name,
                    Opentime = open,
                    Closetime = close,
                    Location = location,
                };
                this.srepo.AddElement(itemToAdd);
            }
        }

        /// <summary>
        /// Helper method to get all the food and converts them to a list.
        /// </summary>
        /// <returns>list of food.</returns>
        public List<Food> GetAllFoods()
        {
            return this.frepo.GetAllElements().ToList();
        }

        /// <summary>
        /// Helper method to get all the Ingredient and converts them to a list.
        /// </summary>
        /// <returns>list of Ingredient.</returns>
        public List<Ingredient> GetAllIngredients()
        {
            return this.irepo.GetAllElements().ToList();
        }

        /// <summary>
        /// Helper method to get all the Recipe and converts them to a list.
        /// </summary>
        /// <returns>list of Recipe.</returns>
        public List<Recipe> GetAllRecipe()
        {
            return this.rrepo.GetAllElements().ToList();
        }

        /// <summary>
        /// Helper method to get all the Store and converts them to a list.
        /// </summary>
        /// <returns>list of Store.</returns>
        public List<Store> GetAllStores()
        {
            return this.srepo.GetAllElements().ToList();
        }

        /// <summary>
        /// Sends a request to repository to get all data of a food based on it's id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>food.</returns>
        public Food GetFoodById(int id)
        {
            // Listába feltöltjük a létező ID-ket:
            List<int> idlist = new List<int>();
            var food = this.GetAllFoods();

            foreach (var met in food)
            {
                idlist.Add((int)met.Fid);
            }

            if (idlist.Contains(id))
            {
                return this.frepo.GetElementById(id);
            } else
            {
                throw new NoSuchItemInDatabaseExcemtion("This food does not exists in db yet");
            }
        }

        /// <summary>
        /// Sends a request to repository to get all data of a ingredient based on it's id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>food.</returns>
        public Ingredient GetIngredientById(int id)
        {
            // Listába feltöltjük a létező ID-ket:
            List<int> idlist = new List<int>();
            var ingredient = this.GetAllIngredients();

            foreach (var met in ingredient)
            {
                idlist.Add((int)met.Iid);
            }

            if (idlist.Contains(id))
            {
                return this.irepo.GetElementById(id);
            } else
            {
                throw new NoSuchItemInDatabaseExcemtion("This food does not exists in db yet");
            }
        }

        /// <summary>
        /// Sends a request to repository to get all data of a recipe based on it's id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>food.</returns>
        public Recipe GetRecipeById(int id)
        {
            // Listába feltöltjük a létező ID-ket:
            List<int> idlist = new List<int>();
            var recipe = this.GetAllRecipe();

            foreach (var met in recipe)
            {
                idlist.Add((int)met.Rid);
            }

            if (idlist.Contains(id))
            {
                return this.rrepo.GetElementById(id);
            } else
            {
                throw new NoSuchItemInDatabaseExcemtion("This food does not exists in db yet");
            }
        }

        /// <summary>
        /// Sends a request to repository to get all data of a store based on it's id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>food.</returns>
        public Store GetStoreById(int id)
        {
            // Listába feltöltjük a létező ID-ket:
            List<int> idlist = new List<int>();
            var store = this.GetAllStores();

            foreach (var met in store)
            {
                idlist.Add((int)met.Sid);
            }

            if (idlist.Contains(id))
            {
                return this.srepo.GetElementById(id);
            } else
            {
                throw new NoSuchItemInDatabaseExcemtion("This food does not exists in db yet");
            }
        }

        /// <summary>
        /// Send a request to repository to update a food in the database.
        /// </summary>
        /// <param name="id">oldid.</param>
        /// <param name="name">name.</param>
        /// <param name="servings">servcing.</param>
        /// <param name="bestb4">bestb4.</param>
        /// <param name="type">type.</param>
        /// <param name="vega">vega.</param>
        public void UpdateFood(int id, string name, int servings, int bestb4, string type, int vega)
        {
            if (name.Length == 0 || servings == 0)
            {
                throw new WrongArgumentException("Food cant be created");
            }
            else
            {
                // Id check:
                List<int> idlist = new List<int>();
                var food = this.GetAllFoods();

                foreach (var item in food)
                {
                    idlist.Add((int)item.Fid);
                }

                if (idlist.Contains(id))
                {
                    Food newItemToAdd = new Food()
                    {
                        Fid = id,
                        Name = name,
                        Servings = servings,
                        BestB4 = bestb4,
                        Type = type,
                        Vega = vega,
                    };
                    this.frepo.UpdateElement(newItemToAdd);
                } else
                {
                    throw new NoSuchItemInDatabaseExcemtion("This food does not exists in db yet");
                }
            }
        }

        /// <summary>
        /// Send a request to repository to update a ingredient in the database.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="sid">sid.</param>
        /// <param name="name">namne.</param>
        /// <param name="unit">unit.</param>
        /// <param name="type">type.</param>
        public void UpdateIngredient(int id, int sid, string name, string unit, string type)
        {
            if (!this.srepo.FindId(sid))
            {
                throw new NoSuchItemInDatabaseExcemtion("The store does not exists yet in db");
            }
            else if (name.Length == 0 || unit.Length == 0)
            {
                throw new WrongArgumentException("Ingredient cant be created");
            }
            else
            {
                // Id check:
                List<int> idlist = new List<int>();
                var ingredient = this.GetAllIngredients();

                foreach (var item in ingredient)
                {
                    idlist.Add((int)item.Iid);
                }

                if (idlist.Contains(id))
                {
                    Ingredient newItemToAdd = new Ingredient()
                    {
                        Iid = id,
                        Sid = sid,
                        Name = name,
                        Unit = unit,
                        Type = type,
                    };
                    this.irepo.UpdateElement(newItemToAdd);
                } else
                {
                    throw new NoSuchItemInDatabaseExcemtion("This Ingredient does not exists in db yet");
                }
            }
        }

        /// <summary>
        /// Send a request to repository to update a Recipe in the database.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="fid">sid.</param>
        /// <param name="iid">namne.</param>
        /// <param name="quantity">unit.</param>
        public void UpdateRecipe(int id, int fid, int iid, int quantity)
        {
            if (!this.frepo.FindId(fid))
            {
                throw new NoSuchItemInDatabaseExcemtion("This food does not exists in db yet");
            }
            else if (!this.irepo.FindId(iid))
            {
                throw new NoSuchItemInDatabaseExcemtion("This ingredient does not exists in db yet");
            }
            else
            {
                // Id check:
                List<int> idlist = new List<int>();
                var recipe = this.GetAllRecipe();

                foreach (var item in recipe)
                {
                    idlist.Add((int)item.Rid);
                }

                if (idlist.Contains(id))
                {
                    Recipe newItemToAdd = new Recipe()
                    {
                        Rid = id,
                        Fid = fid,
                        Iid = iid,
                        Quantity = quantity,
                    };
                    this.rrepo.UpdateElement(newItemToAdd);
                } else
                {
                    throw new NoSuchItemInDatabaseExcemtion("This Recepie does not exists in db yet");
                }
            }
        }

        /// <summary>
        /// Send a request to repository to update a Recipe in the database.
        /// </summary>
        /// <param name="id">id.</param>
        /// <param name="name">name,.</param>
        /// <param name="open">open.</param>
        /// <param name="close">close.</param>
        /// <param name="location">loction.</param>
        public void UpdateStore(int id, string name, DateTime open, DateTime close, string location)
        {
            TimeSpan openhours = close - open;

            if (name.Length == 0)
            {
                throw new WrongArgumentException("Store cant be created with this name");
            }
            else if (openhours.Hours <= 0)
            {
                throw new WrongArgumentException("Open cant be greater than close");
            }
            else
            {
                // Id check:
                List<int> idlist = new List<int>();
                var store = this.GetAllStores();

                foreach (var item in store)
                {
                    idlist.Add((int)item.Sid);
                }

                if (idlist.Contains(id))
                {
                    Store itemToAdd = new Store()
                    {
                        Sid = this.srepo.LastId() + 1,
                        Name = name,
                        Opentime = open,
                        Closetime = close,
                        Location = location,
                    };
                    this.srepo.UpdateElement(itemToAdd);
                } else
                {
                    throw new NoSuchItemInDatabaseExcemtion("This Store does not exists in db yet");
                }
            }
        }

        /// <summary>
        /// Sends a request to repository to delete a food.
        /// </summary>
        /// <param name="id">id.</param>
        public void DeleteFood(int id)
        {
            List<int> idlist = new List<int>();
            var foods = this.GetAllFoods();
            foreach (var food in foods)
            {
                idlist.Add((int)food.Fid);
            }

            if (idlist.Contains(id))
            {
                this.frepo.DeleteElement(id);
            } else
            {
                throw new NoSuchItemInDatabaseExcemtion("This Store does not exists in db yet");
            }
        }

        /// <summary>
        /// Sends a request to repository to delete an Ingredient.
        /// </summary>
        /// <param name="id">id.</param>
        public void DeleteIngredient(int id)
        {
            List<int> idlist = new List<int>();
            var ingredients = this.GetAllIngredients();
            foreach (var item in ingredients)
            {
                idlist.Add((int)item.Iid);
            }

            if (idlist.Contains(id))
            {
                this.irepo.DeleteElement(id);
            } else
            {
                throw new NoSuchItemInDatabaseExcemtion("This Store does not exists in db yet");
            }
        }

        /// <summary>
        /// Sends a request to repository to delete a Recipe.
        /// </summary>
        /// <param name="id">id.</param>
        public void DeleteRecepie(int id)
        {
            List<int> idlist = new List<int>();
            var recepies = this.GetAllRecipe();
            foreach (var item in recepies)
            {
                idlist.Add((int)item.Rid);
            }

            if (idlist.Contains(id))
            {
                this.rrepo.DeleteElement(id);
            } else
            {
                throw new NoSuchItemInDatabaseExcemtion("This Store does not exists in db yet");
            }
        }

        /// <summary>
        /// Sends a request to repository to delete a store.
        /// </summary>
        /// <param name="id">id.</param>
        public void DeleteStore(int id)
        {
            List<int> idlist = new List<int>();
            var stores = this.GetAllStores();
            foreach (var item in stores)
            {
                idlist.Add((int)item.Sid);
            }

            if (idlist.Contains(id))
            {
                this.srepo.DeleteElement(id);
            } else
            {
                throw new NoSuchItemInDatabaseExcemtion("This Store does not exists in db yet");
            }
        }

        /// <summary>
        /// Creates  list.
        /// </summary>
        /// <returns>Lit of food types.</returns>
        public List<string> FoodTypes()
        {
            List<string> types = new List<string>();
            var foods = this.GetAllFoods();
            foreach (var food in foods)
            {
                if (!types.Contains(food.Type))
                {
                    types.Add(food.Type);
                }
            }

            return types;
        }

        /// <summary>
        /// Creates  list.
        /// </summary>
        /// <returns>Lit of ingredient types.</returns>
        public List<string> IngredientTypes()
        {
            List<string> types = new List<string>();
            var ingredeients = this.GetAllIngredients();
            foreach (var ingredient in ingredeients)
            {
                if (!types.Contains(ingredient.Type))
                {
                    types.Add(ingredient.Type);
                }
            }

            return types;
        }

        /// <summary>
        /// Creates  list.
        /// </summary>
        /// <param name="foodId">id.</param>
        /// <returns>ingredients from  recipe.</returns>
        public IQueryable<Ingredient> IngredientsFromRecipe(int foodId)
        {
            var recipes = this.rrepo.GetAllElements();
            var ingredients = this.irepo.GetAllElements();
            var q = from x in recipes join y in ingredients on x.Iid equals y.Iid where x.Fid == foodId select y;
            return q;
        }
    }
}
