﻿namespace MealPrepSunday.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class neded for writng to file.
    /// </summary>
    public class ShoppingListElement
    {
        /// <summary>
        /// Gets or sets stores Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets stores Unit.
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// Gets or sets stores Quantity.
        /// </summary>
        public double Quantity { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ShoppingListElement"/> class.
        /// </summary>
        /// <param name="n">Vegeterian.</param>
        /// <param name="u">Breakfast.</param>
        /// <param name="q">MaxMealRepeats.</param>
        public ShoppingListElement(string n, string u, double q)
        {
            this.Name = n;
            this.Unit = u;
            this.Quantity = q;
        }
    }
}
