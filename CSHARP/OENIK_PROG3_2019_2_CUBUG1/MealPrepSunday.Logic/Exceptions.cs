﻿// <copyright file=Exceptions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace MealPrepSunday.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// simple exceptsion for this project.
    /// </summary>
    public class WrongArgumentException : Exception
    {
        public string ErrorMessege { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="WrongArgumentException"/> class.
        /// </summary>
        public WrongArgumentException(string error)
        {
            this.ErrorMessege = error;
        }
    }

    /// <summary>
    /// A simple exceptsion for this project.
    /// </summary>
    public class NoSuchItemInDatabaseExcemtion : Exception
    {
        public string ErrorMessege { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NoSuchItemInDatabaseExcemtion"/> class.
        /// </summary>
        public NoSuchItemInDatabaseExcemtion(string error)
        {
            this.ErrorMessege = error;
        }
    }
}
