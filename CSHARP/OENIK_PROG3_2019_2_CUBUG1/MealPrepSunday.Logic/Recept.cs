﻿// <copyright file="Recept.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MealPrepSunday.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Small class for storing stuff from web.
    /// </summary>
    public class Recept
    {
        /// <summary>
        /// Gets or sets stores Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets stores Unit.
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// Gets or sets stores Quantity.
        /// </summary>
        public int Quantity { get; set; }
    }
}
