﻿// <copyright file="StoreRepository.cs" company="LmaoYeet">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MealPrepSunday.Data;

namespace MealPrepSunday.Repository
{
    /// <summary>
    /// Store repository handler.
    /// </summary>
    public class StoreRepository : IRepository<Store>
    {
        private DATA db;

        /// <summary>
        /// Initializes a new instance of the <see cref="StoreRepository"/> class.
        /// </summary>
        public StoreRepository()
        {
            this.db = new DATA();
        }

        /// <summary>
        /// Inserts new Store into the database.
        /// </summary>
        /// <param name="newelement">Store.</param>
        public void AddElement(Store newelement)
        {
            this.db.Stores.Add(newelement);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Finds a Food based on it's id.
        /// </summary>
        /// <param name="id">unique id of a Food.</param>
        /// <returns>Food.</returns>
        public Store GetElementById(int id)
        {
            return this.db.Stores.FirstOrDefault(x => x.Sid == id);
        }

        /// <summary>
        /// Deletes Store from the database based on it's id.
        /// </summary>
        /// <param name="id">unique id of the Store.</param>
        public void DeleteElement(int id)
        {
            this.db.Stores.Remove(this.GetElementById(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Returns all Stores as queryable.
        /// </summary>
        /// <returns>Stores.</returns>
        public IQueryable<Store> GetAllElements()
        {
            return this.db.Stores;
        }

        /// <summary>
        /// Finds a Store based on it's id.
        /// </summary>
        /// <param name="id">unique id of a Store.</param>
        /// <returns>Store.</returns>

        /// <summary>
        /// Returns the highest id (latest) from the Store database.
        /// </summary>
        /// <returns>integer.</returns>
        public int LastId()
        {
            return (int)this.db.Stores.Max(x => x.Sid);
        }

        /// <summary>
        /// Updates a Store in the Store database.
        /// </summary>
        /// <param name="newelement">meteorologist.</param>
        public void UpdateElement(Store newelement)
        {
            this.db.Stores.AddOrUpdate(newelement);
        }

        /// <summary>
        /// Checks if the item exists in the db.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>True/False.</returns>
        public bool FindId(int id)
        {
            return this.GetAllElements().Any(x => x.Sid == id);
        }
    }
}
