﻿namespace MealPrepSunday.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using MealPrepSunday.Data;

    public class DataBaseRepository
    { 
        private DATAEntities db;            

        public DataBaseRepository()
        {
            this.db = DataBaseHandler.DB;
        }

        /// <summary> Adds a new FOOD object to the database. </summary>
        /// <param name="newfood">The object to add.</param>
        public void AddFood(FOOD newfood)
        {
            this.db.FOODs.Add(newfood);
            this.db.SaveChanges();
        }

        /// <summary> Adds a new RECEPIE object to the database. </summary>
        /// <param name="newrecipe">The object to add.</param>
        public void AddRecipe(RECEPIE newrecipe)
        {
            this.db.RECEPIEs.Add(newrecipe);
            this.db.SaveChanges();
        }

        /// <summary> Adds a new STORETYPE object to the database. </summary>
        /// <param name="newstoretype">The object to add.</param>
        public void AddStoreType(STORETYPE newstoretype)
        {
            this.db.STORETYPEs.Add(newstoretype);
            this.db.SaveChanges();
        }

        /// <summary> Adds a new INGREDIENT object to the database. </summary>
        /// <param name="newingredient">The object to add.</param>
        public void AddIngredient(INGREDIENT newingredient)
        {
            this.db.INGREDIENTs.Add(newingredient);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Adds a new FOOD object to the database.
        /// </summary>
        /// <param name="id">The id of the desired object.</param>
        /// <returns>First item with this id.</returns>
        public FOOD GetStoretypeById(int id)
        {
            return this.db.FOODs.FirstOrDefault(t => t.FID == id);
        }

        /// <summary> Adds a new RECEPIE object to the database. </summary>
        /// <param name="id">The id of the desired object.</param>
        /// <returns>First item with this id.</returns>
        public RECEPIE GetRecepieById(int id)
        {
            throw new NotImplementedException("mi van az osszetett kulccsal???");
            return this.db.RECEPIEs.FirstOrDefault(t => t.FID == id);
        }

        /// <summary> Adds a new STORETYPE object to the database. </summary>
        /// <param name="id">The id of the desired object.</param>
        /// <returns>First item with this id.</returns>
        public STORETYPE GetStoreTypeById(int id)
        {
            return this.db.STORETYPEs.FirstOrDefault(t => t.SID == id);
        }

        /// <summary> Adds a new INGREDIENT object to the database. </summary>
        /// <param name="id">The id of the desired object.</param>
        /// <returns>First item with this id.</returns>
        public INGREDIENT GetIngredientById(int id)
        {
            return this.db.INGREDIENTs.FirstOrDefault(t => t.IID == id);
        }

        /// <summary> Deletes the first item with this id. </summary>
        /// <param name="id">id of the item to delete.</param>
        public void DeleteFood(int id)
        {
            this.db.FOODs.Remove(this.GetStoretypeById(id));
            this.db.SaveChanges();
        }

        /// <summary> Deletes the first item with this id. </summary>
        /// <param name="id">id of the item to delete.</param>
        public void DeleteRecepie(int id)
        {
            this.db.RECEPIEs.Remove(this.GetRecepieById(id));
            this.db.SaveChanges();
        }

        /// <summary> Deletes the first item with this id. </summary>
        /// <param name="id">id of the item to delete.</param>
        public void DeleteStoreType(int id)
        {
            this.db.FOODs.Remove(this.GetStoretypeById(id));
            this.db.SaveChanges();
        }

        /// <summary> Deletes the first item with this id. </summary>
        /// <param name="id">id of the item to delete.</param>
        public void DeleteIngredient(int id)
        {
            this.db.INGREDIENTs.Remove(this.GetIngredientById(id));
            this.db.SaveChanges();
        }

        /// <summary> Updates item in database. </summary>
        /// <param name="oldid">id of item to delete.</param>
        /// <param name="newItem">new item to add to database.</param>
        public void UpdateFood(int oldid, FOOD newItem)
        {
            DeleteFood(oldid);
            AddFood(newItem);
        }

        /// <summary> Updates item in database. </summary>
        /// <param name="oldid">id of item to delete.</param>
        /// <param name="newItem">new item to add to database.</param>
        public void UpdateRecepie(int oldid, RECEPIE newItem) {
            DeleteRecepie(oldid);
            AddRecipe(newItem);
        }

        /// <summary> Updates item in database. </summary>
        /// <param name="oldid">id of item to delete.</param>
        /// <param name="newItem">new item to add to database.</param>
        public void UpdateStoreType(int oldid, STORETYPE newItem) {
            DeleteStoreType(oldid);
            AddStoreType(newItem);
        }

        /// <summary> Updates item in database. </summary>
        /// <param name="oldid">id of item to delete.</param>
        /// <param name="newItem">new item to add to database.</param>
        public void UpdateIngredient(int oldid, INGREDIENT newItem) {
            DeleteIngredient(oldid);
            AddIngredient(newItem);
        }

        /// <summary> Returns all item from database. </summary>
        public IQueryable<FOOD> GetAllFood()
        {
            return this.db.FOODs;
        }

        /// <summary>Returns all item from database.</summary>
        public IQueryable<RECEPIE> GetAllRecepie() {
            return this.db.RECEPIEs;
        }

        /// <summary> Returns all item from database. </summary>
        public IQueryable<STORETYPE> GetAllStoretype() {
            return this.db.STORETYPEs;
        }

        /// <summary> Returns all item from database. </summary>
        public IQueryable<INGREDIENT> GetAllIngredeint() {
            return this.db.INGREDIENTs;
        }

        /// <summary>  Returns the next free id number. </summary>
        /// <returns> Last food id.</returns>
        public int LastFoodId()
        {
            return (int)this.db.FOODs.Max(t => t.FID);
        }

        /// <summary>  Returns the next free id number. </summary>
        /// <returns> Last recepie id.</returns>
        public int LastRecepieId() {
            throw new NotImplementedException("osszetett kulcs wtf")
            return (int)this.db.RECEPIEs.Max(t => t.FID);
        }

        /// <summary>  Returns the next free id number. </summary>
        /// <returns> Last storetype id.</returns>
        public int LastStoretypeId() {
            return (int)this.db.STORETYPEs.Max(t => t.SID);
        }

        /// <summary>  Returns the next free id number. </summary>
        /// <returns> Last ingredient id.</returns>
        public int LastIngredientId() {
            return (int)this.db.INGREDIENTs.Max(t => t.IID);
        }
    }
}
