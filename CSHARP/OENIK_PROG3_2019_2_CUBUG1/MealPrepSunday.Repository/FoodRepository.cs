﻿// <copyright file="FoodRepository.cs" company="LmaoYeet">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MealPrepSunday.Data;

namespace MealPrepSunday.Repository
{
    /// <summary>
    /// Food repository handler.
    /// </summary>
    public class FoodRepository : IRepository<Food>
    {
        private DATA db;

        /// <summary>
        /// Initializes a new instance of the <see cref="FoodRepository"/> class.
        /// </summary>
        public FoodRepository()
        {
            this.db = new DATA();
        }

        /// <summary>
        /// Inserts new Food into the database.
        /// </summary>
        /// <param name="newelement">Food.</param>
        public void AddElement(Food newelement)
        {
            this.db.Foods.Add(newelement);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes Food from the database based on it's id.
        /// </summary>
        /// <param name="id">unique id of the Food.</param>
        public void DeleteElement(int id)
        {
            this.db.Foods.Remove(this.GetElementById(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Returns all Foods as queryable.
        /// </summary>
        /// <returns>Foods.</returns>
        public IQueryable<Food> GetAllElements()
        {
            return this.db.Foods;
        }

        /// <summary>
        /// Finds a Food based on it's id.
        /// </summary>
        /// <param name="id">unique id of a Food.</param>
        /// <returns>Food.</returns>
        public Food GetElementById(int id)
        {
            var result = this.db.Foods.FirstOrDefault(x => x.Fid == id);
            return result;
        }

        /// <summary>
        /// Returns the highest id (latest) from the Food database.
        /// </summary>
        /// <returns>integer.</returns>
        public int LastId()
        {
            return (int)this.db.Foods.Max(x => x.Fid);
        }

        /// <summary>
        /// Updates a Food in the Food database.
        /// </summary>
        /// <param name="newelement">meteorologist.</param>
        public void UpdateElement(Food newelement)
        {
            this.db.Foods.AddOrUpdate(newelement);
        }

        /// <summary>
        /// Checks if the item exists in the db.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>True/False.</returns>
        public bool FindId(int id)
        {
            return this.GetAllElements().Any(x => x.Fid == id);
        }
    }
}
