﻿// <copyright file="RecipeRepository.cs" company="LmaoYeet">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MealPrepSunday.Data;

namespace MealPrepSunday.Repository
{
    /// <summary>
    /// Recipe repository handler.
    /// </summary>
    public class RecipeRepository : IRepository<Recipe>
    {
        private DATA db;

        /// <summary>
        /// Initializes a new instance of the <see cref="RecipeRepository"/> class.
        /// </summary>
        public RecipeRepository()
        {
            this.db = new DATA();
        }

        /// <summary>
        /// Inserts new Recipe into the database.
        /// </summary>
        /// <param name="newelement">Recipe.</param>
        public void AddElement(Recipe newelement)
        {
            this.db.Recipes.Add(newelement);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes Recipe from the database based on it's id.
        /// </summary>
        /// <param name="id">unique id of the Recipe.</param>
        public void DeleteElement(int id)
        {
            this.db.Recipes.Remove(this.GetElementById(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Returns all Recipes as queryable.
        /// </summary>
        /// <returns>Recipes.</returns>
        public IQueryable<Recipe> GetAllElements()
        {
            return this.db.Recipes;
        }

        /// <summary>
        /// Finds a Recipe based on it's id.
        /// </summary>
        /// <param name="id">unique id of a Recipe.</param>
        /// <returns>Recipe.</returns>
        public Recipe GetElementById(int id)
        {
            return this.db.Recipes.FirstOrDefault(x => x.Rid == id);
        }

        /// <summary>
        /// Returns the highest id (latest) from the Recipe database.
        /// </summary>
        /// <returns>integer.</returns>
        public int LastId()
        {
            return (int)this.db.Recipes.Max(x => x.Rid);
        }

        /// <summary>
        /// Updates a Recipe in the Recipe database.
        /// </summary>
        /// <param name="newelement">meteorologist.</param>
        public void UpdateElement(Recipe newelement)
        {
            this.db.Recipes.AddOrUpdate(newelement);
        }

        /// <summary>
        /// Checks if the item exists in the db.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>True/False.</returns>
        public bool FindId(int id)
        {
            return this.GetAllElements().Any(x => x.Rid == id);
        }
    }
}
