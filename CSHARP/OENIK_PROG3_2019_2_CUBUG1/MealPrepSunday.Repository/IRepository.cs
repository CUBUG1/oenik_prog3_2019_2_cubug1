﻿// <copyright file="IRepository.cs" company="LmaoYeet">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MealPrepSunday.Repository
{
    using System.Linq;

    /// <summary>
    /// Initialization interface for the Repositories.
    /// </summary>
    /// <typeparam name="T">Any type of repository.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Adds a new element to the database.
        /// </summary>
        /// <param name="newelement">element type depends on repository.</param>
        void AddElement(T newelement);

        /// <summary>
        /// Deletes element from the database.
        /// </summary>
        /// <param name="id">unique id of the element.</param>
        void DeleteElement(int id);

        /// <summary>
        /// Returns all elements from the database.
        /// </summary>
        /// <returns>elements.</returns>
        IQueryable<T> GetAllElements();

        /// <summary>
        /// Returns an element based on it's id.
        /// </summary>
        /// <param name="id">id of an element.</param>
        /// <returns>element.</returns>
        T GetElementById(int id);

        /// <summary>
        /// Helper method to find the highest (latest used) id.
        /// </summary>
        /// <returns>integer.</returns>
        int LastId();

        /// <summary>
        /// Updates an element in the database.
        /// </summary>
        /// <param name="newelement">element type depends on repository.</param>
        void UpdateElement(T newelement);

        /// <summary>
        /// Checks if the item exists in the db.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>True/False.</returns>
        bool FindId(int id);
    }
}
