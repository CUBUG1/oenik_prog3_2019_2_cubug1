﻿// <copyright file="IngredientRepository.cs" company="LmaoYeet">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MealPrepSunday.Data;

namespace MealPrepSunday.Repository
{
    /// <summary>
    /// Ingredient repository handler.
    /// </summary>
    public class IngredientRepository : IRepository<Ingredient>
    {
        private DATA db;

        /// <summary>
        /// Initializes a new instance of the <see cref="IngredientRepository"/> class.
        /// </summary>
        public IngredientRepository()
        {
            this.db = new DATA();
        }

        /// <summary>
        /// Inserts new Ingredient into the database.
        /// </summary>
        /// <param name="newelement">Ingredient.</param>
        public void AddElement(Ingredient newelement)
        {
            this.db.Ingredients.Add(newelement);
            this.db.SaveChanges();
        }

        /// <summary>
        /// Deletes Ingredient from the database based on it's id.
        /// </summary>
        /// <param name="id">unique id of the Ingredient.</param>
        public void DeleteElement(int id)
        {
            this.db.Ingredients.Remove(this.GetElementById(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Returns all Ingredients as queryable.
        /// </summary>
        /// <returns>Ingredients.</returns>
        public IQueryable<Ingredient> GetAllElements()
        {
            return this.db.Ingredients;
        }

        /// <summary>
        /// Finds a Ingredient based on it's id.
        /// </summary>
        /// <param name="id">unique id of a Ingredient.</param>
        /// <returns>Ingredient.</returns>
        public Ingredient GetElementById(int id)
        {
            return this.db.Ingredients.FirstOrDefault(x => x.Iid == id);
        }

        /// <summary>
        /// Returns the highest id (latest) from the Ingredient database.
        /// </summary>
        /// <returns>integer.</returns>
        public int LastId()
        {
            return (int)this.db.Ingredients.Max(x => x.Iid);
        }

        /// <summary>
        /// Updates a Ingredient in the Ingredient database.
        /// </summary>
        /// <param name="newelement">meteorologist.</param>
        public void UpdateElement(Ingredient newelement)
        {
            this.db.Ingredients.AddOrUpdate(newelement);
        }

        /// <summary>
        /// Checks if the item exists in the db.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>True/False.</returns>
        public bool FindId(int id)
        {
            return this.GetAllElements().Any(x => x.Iid == id);
        }
    }
}
