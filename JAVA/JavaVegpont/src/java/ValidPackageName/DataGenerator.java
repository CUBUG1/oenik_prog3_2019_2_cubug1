/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ValidPackageName;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

/**
 *
 * @author Peti
 */

public class DataGenerator {
    private List<Termek> termekek = new ArrayList<Termek>();
    private static final Random random = new Random();
    public DataGenerator(){
        this.termekek.add(new Termek("répa", "db"));
        this.termekek.add(new Termek("kenyér", "kg"));
        this.termekek.add(new Termek("tej", "l"));
        this.termekek.add(new Termek("tejföl", "dkg"));
        this.termekek.add(new Termek("párizsi", "dkg"));
        this.termekek.add(new Termek("sajt", "dkg"));
        this.termekek.add(new Termek("alma", "db"));
        this.termekek.add(new Termek("krumpli", "kg"));
        this.termekek.add(new Termek("avokádó", "db"));
        this.termekek.add(new Termek("banán", "db"));
        this.termekek.add(new Termek("pizza", "db"));
        this.termekek.add(new Termek("víz", "l"));
        this.termekek.add(new Termek("barack", "db"));
    }
    public String getJson(String user) {
        JsonObjectBuilder jobj = Json.createObjectBuilder();
        int r = random.nextInt((10 - 0) + 1);
            jobj.add("Name", termekek.get(r).getName());
            jobj.add("Unit", termekek.get(r).getUnit());
            jobj.add("Quantity",  Integer.toString(random.nextInt((10 - 0) + 1)));        
        JsonObject complete = jobj.build();
        return complete.toString();
    }
}
