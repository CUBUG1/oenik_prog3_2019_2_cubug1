/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ValidPackageName;

/**
 *
 * @author Peti
 */
public class Termek {

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getUnit() {
        return Unit;
    }

    public void setUnit(String Unit) {
        this.Unit = Unit;
    }

    public Termek(String Name, String Unit) {
        this.Name = Name;
        this.Unit = Unit;
    }
    private String Name;
    private String Unit;
}
